//
//  signupViewController.swift
//  STYLEPLUSPROJECT
//
//  Created by WhiteOval mac on 14/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit





class signupViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
  
    @IBOutlet weak var fullnametxt: UITextField!
    @IBOutlet weak var emailtxt: UITextField!
    @IBOutlet weak var mobilenumbertxt: UITextField!
    @IBOutlet weak var agetxt: UITextField!
     @IBOutlet weak var dpimage: UIImageView!
    @IBAction func genderbtn(_ sender: UIButton) {
        
        if sender.tag == 1{
            print("male")
        }
        else {
            
            print("female")
        }
        
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        dpimage.image = image
        dpimage.contentMode = .scaleAspectFit
        
        dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = .photoLibrary
        present(controller, animated: true, completion: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    @IBAction func signupbtn(_ sender: UIButton) {
        
        let dict = ["username": "\(fullnametxt.text)", "email":"\(emailtxt.text)","phoneNumber":"7777777778","dob":"1995-05-05","gender":"male","mallId":"66"] as [String: Any]
        
        
        if let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: []) {
            
            
            let url = NSURL(string: "http://34.238.104.190:8080/stylePlusV2/webService/appSignUp.jsp")!
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "POST"
            
            request.httpBody = jsonData
            
            let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
                if error != nil{
                    print(error?.localizedDescription)
                    return
                }
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                    
                    if let parseJSON = json {
                        
                      if parseJSON.object(forKey: "status") as! String == "success"
                      {
                        
                            let viewcontrollerp = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! loginViewController
                            self.navigationController?.pushViewController(viewcontrollerp, animated: true)
                        
                        }
                        print(parseJSON)
                    }
                } catch let error as NSError {
                    print(error)
                }
            }
            task.resume()
        }
        
        let viewcontrollerp = storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! loginViewController
      
             self.navigationController?.pushViewController(viewcontrollerp, animated: true)
        
        
        
        
        
//       //http://34.238.104.190:8080/stylePlusV2/webService/appSignUp.jsp
//
//        //var request = URLRequest(url: URL(string: "http://34.238.104.190:8080/stylePlusV2/webService/appSignUp.jsp")!)
//       // request.httpMethod = "POST"
//      // let parameters = ["username:\(fullnametxt.text!)&email:\(emailtxt.text!)&phoneNumber:\(mobilenumbertxt.text!)&dob:1993-05-05&gender:male&mallId:66"]
//       // request.httpBody = postString.data(using: .utf8)
//
//        let postString = "username=@kilo_loco&email=HelloWorld&phoneNumber =435353553535&dob= 19995-05-05&gender=male&mallId=66"
//
//        let url = URL(string: "http://34.238.104.190:8080/stylePlusV2/webService/appSignUp.jsp")
//        var request = URLRequest(url: url!)
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.httpMethod = "POST"
//       // let postString = "id=13&name=Jack"
//        request.httpBody = postString.data(using: .utf8)
//        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//            guard let data = data, error == nil else {                                                 // check for fundamental networking error
//                print("error=\(error)")
//                return
//            }
//
//            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
//                print("statusCode should be 200, but is \(httpStatus.statusCode)")
//                print("response = \(response?.description)")
//            }
//
//            let responseString = String(data: data, encoding: .utf8)
//            print("responseString = \(responseString)")
//        }
//        task.resume()
//
//
//        let viewcontrollerp = storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! loginViewController
//
//        self.navigationController?.pushViewController(viewcontrollerp, animated: true)
        
        
    }
        
   
    @IBAction func loginbutton(_ sender: UIButton) {
    }
    
}
