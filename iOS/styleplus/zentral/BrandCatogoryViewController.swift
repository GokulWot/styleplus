//
//  BrandCatogoryViewController.swift
//  StylePlusproj
//
//  Created by WhiteOval mac on 08/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
import ObjectMapper
import Kingfisher
import ACProgressHUD_Swift

class BrandCatogoryViewController: UIViewController {

@IBOutlet weak var catagoriescollectionview: UICollectionView!
    
    var categoryDataArray : [CategoryData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        ACProgressHUD.shared.showHUD()
        getCategoryData()
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension BrandCatogoryViewController {
    
    
    //get brand category data from api
    func getCategoryData() {
        
        ApiCalls.getCategoryData(success: { (jsonArray) in
            
            self.categoryDataArray = Mapper<CategoryData>().mapArray(JSONArray: jsonArray)
            ACProgressHUD.shared.hideHUD()
            self.catagoriescollectionview.reloadData()
        }) { (error) in
            
            print(error.localizedDescription)
            
        }
        
    }
   
}

extension BrandCatogoryViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let measure = (self.view.frame.size.width/2) - 40 //some width
        return CGSize(width: measure, height: measure)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
   
        
        if let count = self.categoryDataArray?.count{
            
            return count
            
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatagoriesCollectionViewCell", for: indexPath) as! CatagoriesCollectionViewCell
        
        cell.catimg?.kf.setImage(with: URL(string: self.categoryDataArray![indexPath.row].categoryIcon!))
        cell.catognamelbl.text = self.categoryDataArray![indexPath.row].categoryName
        
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewcontroller6 = storyboard?.instantiateViewController(withIdentifier: "AllBrandViewController") as! AllBrandViewController
        
        viewcontroller6.brandCategoryData = self.categoryDataArray![indexPath.row]
        
        self.navigationController?.pushViewController(viewcontroller6, animated: true)
        
    }
}
