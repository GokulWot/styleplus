//
//  SignupViewController.swift
//  STYLEPLUSPROJECT
//
//  Created by WhiteOval mac on 14/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import ACProgressHUD_Swift

class SignupViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate{
  
    @IBOutlet weak var fullNameTxt: UITextField!
    @IBOutlet weak var emaiTlxt: UITextField!
    @IBOutlet weak var mobileNumberTxt: UITextField!
    @IBOutlet weak var ageTxt: UITextField!
    @IBOutlet weak var dpImg: UIImageView!
    @IBOutlet weak var maleRadBtn: DLRadioButton!
    
    @IBOutlet weak var presentlbl: UILabel!
    
    var parameter : Parameters?
    var loginData : LoginData?
    var imguserid : Int?
    let mallId = Constants.GlobalConst.mallId
    
    
     let datePicker = UIDatePicker()
     let imagePickerController = UIImagePickerController()
     var takenimage : String?
     var gender : String = ""
    @IBAction func radioBtnsPressed(_ sender: DLRadioButton) {
      
        if sender.tag == 1{
            print("Male")
            self.gender = "Male"
            
        }
        else if sender.tag == 2 {
            print("Female")
            self.gender = "Female"
        }
        else if sender.tag == 3 {
            print("Trans")
            self.gender = "Trans"
        }
        
        
        
     
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePickerController.delegate = self
      
        showDatePicker()
       
        dpImg.layer.cornerRadius = dpImg.frame.size.width / 2
        
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard)) //hide keyboard on touching superview
    }
   
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        //to add possible dates only
        let currentDate: Date = Date()
        self.datePicker.maximumDate = currentDate
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.bordered, target: self, action:  #selector(self.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.bordered, target: self, action:  #selector(self.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        ageTxt.inputAccessoryView = toolbar
        ageTxt.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        ageTxt.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        
        self.view.endEditing(true)
    }

    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
   //var imageStr = ""
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

    
        let image_data = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
        let imageData:Data = image_data!.pngData()!
        dpImg.contentMode = .scaleAspectFill
        dpImg.image = image_data
        dpImg.clipsToBounds = true
        dismiss(animated: true, completion: nil)
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
       
        
    }
    
    @objc func dismissKeyboard() {
        // do aditional stuff
        view.endEditing(true)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func imageBtnPressed(_ sender: Any) {
        
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = .photoLibrary
        present(controller, animated: true, completion: nil)
    }
    
    
    @IBAction func signupbtn(_ sender: UIButton) {
        
        if(fullNameTxt.text == "" || emaiTlxt.text == "" || mobileNumberTxt.text == "" || ageTxt.text == ""){
            
            var myAlert = UIAlertController(title:"Alert",message:"All fields are required",preferredStyle:UIAlertController.Style.alert)
            let okaction = UIAlertAction(title:"OK",style:UIAlertAction.Style.default,handler:nil)
            myAlert.addAction(okaction)
            
            self.present(myAlert,animated:true,completion: nil)
        }
            
        else {
            
            func isValidEmail(emailID:String) -> Bool {
                let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
                let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
                return emailTest.evaluate(with: emailID)
            }
            
            
            if isValidEmail(emailID: emaiTlxt.text!) == false {
                 presentlbl.isHidden = false
                 presentlbl.text = "Please enter valid email address"
                
                var myAlert = UIAlertController(title:"Alert",message:"Please enter valid email address",preferredStyle:UIAlertController.Style.alert)
                let okaction = UIAlertAction(title:"OK",style:UIAlertAction.Style.default,handler:nil)
                myAlert.addAction(okaction)
                
                self.present(myAlert,animated:true,completion: nil)
            }
                
            else {
                
                func validate(value: String) -> Bool {
                    let PHONE_REGEX = "^[7-9][0-9]{9}$"
                    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
                    let result =  phoneTest.evaluate(with: value)
                    return result
                }
                
                if validate(value: mobileNumberTxt.text!) == false {
                    //presentlbl.isHidden = false
                    //presentlbl.text = "Please enter valid mobilenumber"
                    
                    
                    var myAlert = UIAlertController(title:"Alert",message:"Please enter valid mobile number",preferredStyle:UIAlertController.Style.alert)
                    let okaction = UIAlertAction(title:"OK",style:UIAlertAction.Style.default,handler:nil)
                    myAlert.addAction(okaction)
                    
                    self.present(myAlert,animated:true,completion: nil)
                }
                    
                else {
                    
                    if self.dpImg.image == nil {
                        
                        let myAlert = UIAlertController(title:"Alert",message:"Please add an image",preferredStyle:UIAlertController.Style.alert)
                        let okaction = UIAlertAction(title:"OK",style:UIAlertAction.Style.default,handler:nil)
                        myAlert.addAction(okaction)
                        self.present(myAlert,animated:true,completion: nil)
                    }
                    else {
                        
                        uploadImage()
                    }
                }
            }
        }
    }
}

//MARK:- Extension
private extension SignupViewController {
    
    func uploadImage() {
        
        ACProgressHUD.shared.showHUD()
        let fcmId = UserDefaults.standard.value(forKey:"fcmid") as? String
        parameter = ["username": fullNameTxt.text!, "email":emaiTlxt.text!,"phoneNumber":mobileNumberTxt.text!,"dob":ageTxt.text!,"gender":self.gender,"mallId":mallId,"FCMId":fcmId!]
        
        ApiCalls.signUp(parameters: parameter!, success: { (jsonDict) in
            
            self.loginData = Mapper<LoginData>().map(JSONObject: jsonDict)
            //                print(self.loginData?.status)
            if self.loginData?.status == "success" {
                
                UserDefaults.standard.setValue(true, forKey: "isLoginKey")
                let userId = self.loginData?.userId
                UserDefaults.standard.set(userId, forKey: "userId")
                let imageData = (self.dpImg.image as! UIImage).jpegData(compressionQuality: 0.9)
                let strBase64 = imageData?.base64EncodedString(options: .lineLength64Characters)
                self.takenimage = strBase64
                let baseUrl = Constants.GlobalConst.baseUrl
                
                let imgurlup = baseUrl+"/rest/AppUserImage/upload"
                
                // let imgurlup1 = "http://192.168.0.152:8080/StylePlus/rest/AppUserImage/upload"
                
                Alamofire.request(imgurlup, method: .post, parameters: ["userId": userId!,"image":"\(self.takenimage!)"], headers: nil).responseString{
                    response in
                    switch response.result {
                    case .success(let data):
                        
                        if data == "true" {
                            
                            ACProgressHUD.shared.hideHUD()
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuController")
                            self.present(viewController!, animated: true, completion: nil)
                        }
                        break
                    case .failure(let error):
                        
                        ACProgressHUD.shared.hideHUD()
                        print(error)
                    }
                    
                    
                }
            }
        }) { (error) in
            
            ACProgressHUD.shared.hideHUD()
            print(error.localizedDescription)
        }
    }
}






// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
