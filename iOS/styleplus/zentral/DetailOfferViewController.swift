//
//  DetailOfferViewController.swift
//  StylePlusproj
//
//  Created by WhiteOval mac on 06/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
import ObjectMapper
import ACProgressHUD_Swift

class DetailOfferViewController: UIViewController {
    
    var offerData : OfferData?
    var AllofferData : OfferData?
    var offerDetailsData: Offerdetails?
    var notiOfferData : NotiOfferData?
    var hotDealData : HotDealsData?
    var shopDetailsOfferData : ShopDetailsData?
    var fromView = "self"
    var offerId = Int()
    var userId = Int()
    
    
    @IBOutlet weak var offerImg: UIImageView!
    @IBOutlet weak var shopLogoImg: UIImageView!
    @IBOutlet weak var createdDateLbl: UILabel!
    @IBOutlet weak var expDateLbl: UILabel!
    @IBOutlet weak var discriptLbl: UILabel!
    @IBOutlet weak var hLbl: UILabel!
    @IBOutlet weak var couponLbl: UILabel!
    @IBOutlet weak var backBtn: UIBarButtonItem!    
    @IBOutlet weak var btnLblHeightConstr: NSLayoutConstraint!
    @IBOutlet weak var couponLblHeightConstr: NSLayoutConstraint!
    //var shopimage2 : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetailOfferViewController.tapResponse))
        tapGesture.numberOfTapsRequired = 1
        hLbl.isUserInteractionEnabled =  true
        hLbl.addGestureRecognizer(tapGesture)
        
        if let isLogin : Bool = UserDefaults.standard.value(forKey: "isLoginKey") as? Bool {
            
            if isLogin == false {
                
                btnLblHeightConstr.constant = 0
                couponLblHeightConstr.constant = 0
            }
            else {
                
                btnLblHeightConstr.constant = 50
                couponLblHeightConstr.constant = 50
            }
        }
        else {
            
            btnLblHeightConstr.constant = 0
            couponLblHeightConstr.constant = 0
        }

        if fromView == "notif" {
            
            userId = UserDefaults.standard.value(forKey: "userId") as! Int
            ApiCalls.getFenceData(userId: userId, action: "open", type: "notification", location: "")
            ApiCalls.getNotifData(offerId: self.offerId, success: { (jsonDict) in
                
                self.notiOfferData = Mapper<NotiOfferData>().map(JSONObject: jsonDict)
                ACProgressHUD.shared.showHUD()
                self.setPage()
            }) { (error) in
                
                print(error)
            }
        }
        else {
            
            setPage()
            sideMenuController!.isLeftViewSwipeGestureEnabled = false
            sideMenuController!.isRightViewSwipeGestureEnabled = false
        }
    }
    
    @objc func tapResponse(recognizer: UITapGestureRecognizer) {
        
        if let couponArray = UserDefaults.standard.value(forKey: "couponArray") {
            
            if fromView == "notif" {
                
                let myArray = NSMutableArray(array: couponArray as! [Int])
                myArray.add(self.offerId)
                UserDefaults.standard.set(myArray, forKey: "couponArray")
            }
            else if fromView == "offer" {
                
                let myArray = NSMutableArray(array: couponArray as! [Int])
                myArray.add((offerData?.offerId)!)
                UserDefaults.standard.set(myArray, forKey: "couponArray")
            }
            else if fromView == "shopdeatils" {
                
                let myArray = NSMutableArray(array: couponArray as! [Int])
                myArray.add((offerDetailsData?.offerId)!)
                UserDefaults.standard.set(myArray, forKey: "couponArray")
            }
            else if fromView == "hotDeals" {
                
                let myArray = NSMutableArray(array: couponArray as! [Int])
                myArray.add((hotDealData?.offerId)!)
                UserDefaults.standard.set(myArray, forKey: "couponArray")
            }
            else {
                
                let myArray = NSMutableArray(array: couponArray as! [Int])
                myArray.add((AllofferData?.offerId)!)
                UserDefaults.standard.set(myArray, forKey: "couponArray")
            }
        }
        else {
            
            var couponArray = [Int]()
            if fromView == "notif" {
                
                couponArray.append(self.offerId)
                UserDefaults.standard.set(couponArray, forKey: "couponArray")
            }
            else if fromView == "offer" {
                
                couponArray.append((offerData?.offerId)!)
                UserDefaults.standard.set(couponArray, forKey: "couponArray")
            }
            else if fromView == "shopdeatils" {
                
                couponArray.append((offerDetailsData?.offerId)!)
                UserDefaults.standard.set(couponArray, forKey: "couponArray")
            }
            else if fromView == "hotDeals" {
                
                couponArray.append((hotDealData?.offerId)!)
                UserDefaults.standard.set(couponArray, forKey: "couponArray")
            }
            else {
                
                couponArray.append((AllofferData?.offerId)!)
                UserDefaults.standard.set(couponArray, forKey: "couponArray")
            }
            
        }
        hLbl.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        if fromView == "notif" {
            
            ApiCalls.getFenceData(userId: userId, action: "close", type: "notification", location: "")
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuController")
            self.present(viewController!, animated: true, completion: nil)
        }
        else {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//MARK:- Extension
private extension DetailOfferViewController {
    
    func setPage() {
        
        if fromView == "offer" {
            
            self.offerImg?.kf.setImage(with: URL(string: (offerData?.offerImage!)!)!)
            self.shopLogoImg?.kf.setImage(with: URL(string: (offerData?.shopLogo!)!)!)
            let dateString = offerData?.expiryDate!.capitalized
            let formatter  = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.S"
            let dat : Date = formatter.date(from: dateString!)!
            formatter.dateFormat = "dd-MM-yyyy"
            self.expDateLbl?.text = formatter.string(for: dat)
            self.createdDateLbl?.text = offerData?.offerName!.capitalized
            self.discriptLbl?.text = offerData?.offerDescription!
            self.couponLbl?.text = offerData?.couponCode!
            self.couponCheck(offerId: (offerData?.offerId)!)
        }
        else if fromView == "notif" {
            
            let color = UIColor()
            self.navigationController?.navigationBar.backgroundColor = color.hexStringToUIColor(hex: "290E8F")
            self.offerImg?.kf.setImage(with: URL(string: (notiOfferData?.offerImage!)!)!)
            self.shopLogoImg?.kf.setImage(with: URL(string: (notiOfferData?.shopLogo!)!)!)
            let dateString = notiOfferData?.expiryDate!.capitalized
            let formatter  = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.S"
            let dat : Date = formatter.date(from: dateString!)!
            formatter.dateFormat = "dd-MM-yyyy"
            self.expDateLbl?.text = formatter.string(for: dat)
            self.createdDateLbl?.text = notiOfferData?.offerName!.capitalized
            self.discriptLbl?.text = notiOfferData?.offerDescription!
            self.couponLbl?.text = notiOfferData?.coupon!
            ACProgressHUD.shared.hideHUD()
        }
        else if fromView == "shopdeatils" {
            
            self.offerImg?.kf.setImage(with: URL(string: (offerDetailsData?.offerImage)!)!)
            let dateString = offerDetailsData?.expiryDate!.capitalized
            let formatter  = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.S"
            let dat : Date = formatter.date(from: dateString!)!
            formatter.dateFormat = "dd-MM-yyyy"
            self.expDateLbl?.text = formatter.string(for: dat)
            self.createdDateLbl?.text = offerDetailsData?.offerName!.capitalized
            self.discriptLbl?.text = offerDetailsData?.offerDescription!
            self.couponLbl?.text = offerDetailsData?.coupon!
            self.couponCheck(offerId: (offerDetailsData?.offerId)!)
        }
            
            
        else if fromView == "hotDeals" {
            
            self.offerImg?.kf.setImage(with: URL(string: (hotDealData?.offerImage!)!)!)
            self.shopLogoImg?.kf.setImage(with: URL(string: (hotDealData?.shopLogo!)!)!)
            let dateString = hotDealData?.expiryDate!.capitalized
            let formatter  = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.S"
            let dat : Date = formatter.date(from: dateString!)!
            formatter.dateFormat = "dd-MM-yyyy"
            self.expDateLbl?.text = formatter.string(for: dat)
            self.createdDateLbl?.text = hotDealData?.offerName!.capitalized
            self.discriptLbl?.text = hotDealData?.offerDescription!
            self.couponLbl?.text = hotDealData?.coupon!
            self.couponCheck(offerId: (hotDealData?.offerId)!)
        }
        else {
            
            self.offerImg?.kf.setImage(with: URL(string: (AllofferData?.offerImage!)!)!)
            self.shopLogoImg?.kf.setImage(with: URL(string: (AllofferData?.shopLogo!)!)!)
            let dateString = AllofferData?.expiryDate!.capitalized
            let formatter  = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.S"
            let dat : Date = formatter.date(from: dateString!)!
            formatter.dateFormat = "dd-MM-yyyy"
            self.expDateLbl?.text = formatter.string(for: dat)
            self.createdDateLbl?.text = AllofferData?.offerName!.capitalized
            self.discriptLbl?.text = AllofferData?.offerDescription!
            self.couponLbl?.text = AllofferData?.couponCode!
            self.couponCheck(offerId: (AllofferData?.offerId)!)
        }
    }
    
    func couponCheck(offerId: Int) {
        
        if let couponArray = UserDefaults.standard.value(forKey: "couponArray") {
            
            print(couponArray)
            for item in couponArray as! [Int] {
                
                if item == offerId {
                    
                    hLbl.isHidden = true
                }
            }
        }
    }
}
