

import Foundation
import ObjectMapper

class ShopData : Mappable {
	var subs : [Subs]?
	var categoryName : String?
	var categoryId : Int?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		subs <- map["Subs"]
		categoryName <- map["categoryName"]
		categoryId <- map["categoryId"]
	}

}
