

import Foundation
import ObjectMapper

class Subs : Mappable {
	var shopName : String?
	var shopId : Int?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		shopName <- map["ShopName"]
		shopId <- map["ShopId"]
	}

}
