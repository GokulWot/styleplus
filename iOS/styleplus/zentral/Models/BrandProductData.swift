
// Brand product Model

import Foundation
import ObjectMapper

class BrandProductData : Mappable {
	var itemId : Int?
	var itemName : String?
	var itemDescription : String?
	var itemImage : String?
	var status : String?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		itemId <- map["itemId"]
		itemName <- map["itemName"]
		itemDescription <- map["itemDescription"]
		itemImage <- map["itemImage"]
		status <- map["status"]
	}

}
