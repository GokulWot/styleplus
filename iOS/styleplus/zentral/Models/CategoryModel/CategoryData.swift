

import Foundation
import ObjectMapper

class CategoryData : Mappable {
	var categoryIcon : String?
	var status : String?
	var categoryName : String?
	var categoryId : String?
	var catSubs : [CatSubs]?

    required init?(map: Map) {

	}

    func mapping(map: Map) {

		categoryIcon <- map["categoryIcon"]
		status <- map["Status:"]
		categoryName <- map["categoryName"]
		categoryId <- map["categoryId"]
		catSubs <- map["Subs:"]
	}

}
