
// favourite Model

import Foundation
import ObjectMapper

class FavData : Mappable {
    
	var favLogo : String?
	var favName : String?
	var favDes : String?
	var type : String?
	var favId : Int?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		favLogo <- map["favLogo"]
		favName <- map["favName"]
		favDes <- map["favDes"]
		type <- map["type"]
		favId <- map["favId"]
	}
}
