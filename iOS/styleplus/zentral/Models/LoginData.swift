
// login Model

import Foundation
import ObjectMapper

class LoginData : Mappable {
	var userId : Int?
	var status : String?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		userId <- map["userId"]
		status <- map["status"]
	}

}
