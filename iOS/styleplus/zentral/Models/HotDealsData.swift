
// HotDeals Model

import Foundation
import ObjectMapper

class HotDealsData : Mappable {
	var offerName : String?
	var coupon : String?
	var shopLogo : String?
	var shopImage : String?
	var shopName : String?
	var shopEmail : String?
	var expiryDate : String?
	var createdDate : String?
	var offerDescription : String?
	var offerImage : String?
	var offerId : Int?
	var shopPhone : String?
	var shopId : Int?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		offerName <- map["offerName"]
		coupon <- map["coupon"]
		shopLogo <- map["shopLogo"]
		shopImage <- map["shopImage"]
		shopName <- map["shopName"]
		shopEmail <- map["shopEmail"]
		expiryDate <- map["expiryDate"]
		createdDate <- map["createdDate"]
		offerDescription <- map["offerDescription"]
		offerImage <- map["offerImage"]
		offerId <- map["offerId"]
		shopPhone <- map["shopPhone"]
		shopId <- map["shopId"]
	}

}
