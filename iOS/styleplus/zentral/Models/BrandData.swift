
// Brand Model

import Foundation
import ObjectMapper

class BrandData : Mappable {
    
	var brandName : String?
	var brandDescription : String?
	var brandImage : String?
	var brandId : Int?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		brandName <- map["brandName"]
		brandDescription <- map["brandDescription"]
		brandImage <- map["brandImage"]
		brandId <- map["brandId"]
	}
}
