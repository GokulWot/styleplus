
// Loyalty Model

import Foundation
import ObjectMapper

class LoyaltyData : Mappable {
	var cardType : String?
	var transaction : Int?
	var points : Int?

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		cardType <- map["cardType"]
		transaction <- map["transaction"]
		points <- map["points"]
	}

}
