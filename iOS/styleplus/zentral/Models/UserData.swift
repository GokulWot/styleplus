
// User Model

import Foundation
import ObjectMapper

class UserData : Mappable {
	var userImage : String?
	var gender : String?
	var phone : String?
	var dob : String?
	var name : String?
	var email : String?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		userImage <- map["userImage"]
		gender <- map["gender"]
		phone <- map["phone"]
		dob <- map["dob"]
		name <- map["name"]
		email <- map["email"]
	}

}
