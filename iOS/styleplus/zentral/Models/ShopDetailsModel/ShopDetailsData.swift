

import Foundation
import ObjectMapper

class ShopDetailsData : Mappable {
	var offerdetails : [Offerdetails]?
	var itemdetails : [Itemdetails]?
	var shopdetails : [Shopdetails]?
	var branddetails : [Branddetails]?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		offerdetails <- map["offerdetails"]
		itemdetails <- map["itemdetails"]
		shopdetails <- map["Shopdetails"]
		branddetails <- map["branddetails"]
	}

}
