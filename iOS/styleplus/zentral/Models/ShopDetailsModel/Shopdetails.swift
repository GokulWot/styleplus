

import Foundation
import ObjectMapper

class Shopdetails : Mappable {
	var closingTime : String?
	var shopLogo : String?
	var shopDescription : String?
	var shopImage : String?
	var openingTime : String?
	var shopName : String?
	var shopPhone : String?
	var shopId : Int?
	var shopType : String?
	var shopEmail : String?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		closingTime <- map["closingTime"]
		shopLogo <- map["shopLogo"]
		shopDescription <- map["shopDescription"]
		shopImage <- map["shopImage"]
		openingTime <- map["openingTime"]
		shopName <- map["shopName"]
		shopPhone <- map["shopPhone"]
		shopId <- map["shopId"]
		shopType <- map["shopType"]
		shopEmail <- map["shopEmail"]
	}

}
