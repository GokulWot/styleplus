

import Foundation
import ObjectMapper

class Branddetails : Mappable {
	var brand_image : String?
	var brand_description : String?
	var brand_name : String?
	var brand_id : String?
	var status : String?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		brand_image <- map["brand_image"]
		brand_description <- map["brand_description"]
		brand_name <- map["brand_name"]
		brand_id <- map["brand_id"]
		status <- map["status"]
	}

}
