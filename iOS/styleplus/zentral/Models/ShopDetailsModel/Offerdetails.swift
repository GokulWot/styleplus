

import Foundation
import ObjectMapper

class Offerdetails : Mappable {
    var expiryDate : String?
    var offerName : String?
    var createdDate : String?
    var coupon : String?
    var offerDescription : String?
    var offerImage : String?
    var offerId : Int?
    var status : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        expiryDate <- map["expiryDate"]
        offerName <- map["offerName"]
        createdDate <- map["createdDate"]
        coupon <- map["coupon"]
        offerDescription <- map["offerDescription"]
        offerImage <- map["offerImage"]
        offerId <- map["offerId"]
        status <- map["status"]
    }
    
}
