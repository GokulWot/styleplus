

import Foundation
import ObjectMapper

class Itemdetails : Mappable {
	var item_image : String?
	var item_name : String?
	var item_description : String?
	var status : String?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		item_image <- map["item_image"]
		item_name <- map["item_name"]
		item_description <- map["item_description"]
		status <- map["status"]
	}

}
