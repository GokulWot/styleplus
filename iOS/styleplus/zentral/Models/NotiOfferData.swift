
// Notification offer Model

import Foundation
import ObjectMapper

class NotiOfferData : Mappable {
	var expiryDate : String?
	var offerName : String?
	var createdDate : String?
	var coupon : String?
	var offerDescription : String?
	var offerImage : String?
	var shopLogo : String?
	var shopId : Int?
	var status : String?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		expiryDate <- map["expiryDate"]
		offerName <- map["offerName"]
		createdDate <- map["createdDate"]
		coupon <- map["coupon"]
		offerDescription <- map["offerDescription"]
		offerImage <- map["offerImage"]
		shopLogo <- map["shopLogo"]
		shopId <- map["shopId"]
		status <- map["status"]
	}

}
