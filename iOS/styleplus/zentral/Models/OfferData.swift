
// Offer Model

import Foundation
import ObjectMapper

class OfferData : Mappable {
	var expiryDate : String?
	var offerName : String?
	var createdDate : String?
	var offerDescription : String?
	var offerImage : String?
	var shopLogo : String?
	var offerId : Int?
	var shopName : String?
	var shopId : Int?
	var couponCode : String?

	required init?(map: Map) {

	}

	func mapping(map: Map) {

		expiryDate <- map["expiryDate"]
		offerName <- map["offerName"]
		createdDate <- map["createdDate"]
		offerDescription <- map["offerDescription"]
		offerImage <- map["offerImage"]
		shopLogo <- map["shopLogo"]
		offerId <- map["offerId"]
		shopName <- map["shopName"]
		shopId <- map["shopId"]
		couponCode <- map["couponCode"]
	}

}
