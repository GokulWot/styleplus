//
//  ProductCollectionViewCell.swift
//  StylePlusproj
//
//  Created by WhiteOval mac on 07/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productimg: UIImageView!
    @IBOutlet weak var itemnamelbl: UILabel!
}
