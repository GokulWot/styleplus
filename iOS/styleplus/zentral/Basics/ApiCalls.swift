//
//  ApiCalls.swift
//  Image Slider
//
//  Created by WhiteOval mac on 11/07/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
import Alamofire

class ApiCalls: NSObject {
    
    static let mallId = Constants.GlobalConst.mallId
    
    //login call
    class func logIn(parameters: Parameters, success:@escaping ([String: AnyObject]) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.postUrl("/webService/appLogin.jsp", parameters: parameters, success: {
            (resJson) -> Void in
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    //Signup call
    class func signUp(parameters: Parameters, success:@escaping ([String: AnyObject]) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.postUrl("/webService/appSignUp.jsp", parameters: parameters, success: {
            (resJson) -> Void in
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }

    // Edit userdetails
    
    class func editProfile(parameters: Parameters, success:@escaping ([String: AnyObject]) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.postUrl("/webService/editProfile.jsp", parameters: parameters, success: {
            (resJson) -> Void in
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    //hot deals call
    class func getHotDealsData(success:@escaping (Array<[String: AnyObject]>) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.requestGETURLArry("/webService/hotDeals.jsp?mallId=\(mallId)&limit=1", success: {
            (resJson) -> Void in
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    //offer call
    class func getOffersData(success:@escaping (Array<[String: AnyObject]>) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.requestGETURLArry("/webService/allOffers.jsp?mallId=\(mallId)&offset=0", success: {
            (resJson) -> Void in
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    //brand call
    class func getBrandData(success:@escaping (Array<[String: AnyObject]>) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.requestGETURLArry("/webService/products.jsp?mallId=\(mallId)&limit=6", success: {
            (resJson) -> Void in
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    //brand product call
    class func getBrandProductData(brandId: Int, success:@escaping (Array<[String: AnyObject]>) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.requestGETURLArry("/webService/brandProduct.jsp?mallId=\(mallId)&brandId=\(brandId)", success: {
            (resJson) -> Void in
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    //shop details call
    class func getShopDetailsData(shopId: Int,success:@escaping (Array<[String: AnyObject]>) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.requestGETURLArry("/webService/shopDetails.jsp?id=\(shopId)&mallId=\(mallId)", success: {
            (resJson) -> Void in
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    //shop call
    class func getShopData(success:@escaping (Array<[String: AnyObject]>) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.requestGETURLArry("/webService/stylePlusCategory.jsp?mallId=\(mallId)", success: {
            (resJson) -> Void in
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    //fav check, add and remove call
    class func checkFav(toDo: String,brandId: Int, userId: Int, type: String, success:@escaping ([String: AnyObject]) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.requestGETURLDict("/webService/\(toDo).jsp?mallId=\(mallId)&userId=\(userId)&id=\(brandId)&type=\(type)", success: {
            (resJson) -> Void in
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    //user call
    class func getUserData(userId: Int, success:@escaping (Array<[String: AnyObject]>) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.requestGETURLArry("/webService/userProfile.jsp?userId=\(userId)", success: {
            (resJson) -> Void in
//            print(resJson)
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    //fav call
    class func getFavData(userId: Int, success:@escaping (Array<[String: AnyObject]>) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.requestGETURLArry("/webService/listFavourites.jsp?mallId=\(mallId)&userId=\(userId)", success: {
            (resJson) -> Void in
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    //geofencing entry and exit call
    class func getFenceData(userId: Int, action: String, type: String, location: String) {
        
        AlamofireWrapper.requestGETURLDict("/webService/userLogging.jsp?mallId=\(mallId)&userId=\(userId)&contentId=0&action=\(action)&type=\(type)&location=\(location)", success: {
            (resJson) -> Void in
//            print(resJson)
        }, failure: {
            (error) -> Void in
            print(error)
        })
    }
    
    //notif offer call
    class func getNotifData(offerId: Int, success:@escaping ([String: AnyObject]) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.requestGETURLDict("/webService/offerDetails.jsp?mallId=\(mallId)&id=\(offerId)", success: {
            (resJson) -> Void in
//            print(resJson)
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    // Brandcategory call
    class func getCategoryData(success:@escaping (Array<[String: AnyObject]>) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.requestGETURLArry("/webService/categoryBrands.jsp?mallId=\(mallId)", success: {
            (resJson) -> Void in
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    // category brand call
    class func getCategoryBrandData(categoryId: Int, success:@escaping (Array<[String: AnyObject]>) -> Void,failure:@escaping (Error) -> Void) {
       
        AlamofireWrapper.requestGETURLArry("/webService/brandCategory.jsp?mallId=\(mallId)&categoryId=\(categoryId)", success: { (resJson) -> Void in
//            print(resJson)
            success(resJson)
            
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
    
    // loyalty details call
    class func getLoyalityData(userId: Int, success:@escaping ([String: AnyObject]) -> Void, failure:@escaping (Error) -> Void) {
        
        AlamofireWrapper.requestGETURLDict("/webService/loyalty.jsp?userId=\(userId)", success: {
            (resJson) -> Void in
            success(resJson)
        }, failure: {
            (error) -> Void in
            failure(error)
        })
    }
}

