//
//  AlamofireWrapper.swift
//  Image Slider
//
//  Created by WhiteOval mac on 11/07/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
import Alamofire

class AlamofireWrapper: NSObject {
    
    static let baseUrl = Constants.GlobalConst.baseUrl
    
    //Alamo get Dict
    class func requestGETURLDict(_ urlLast: String, success:@escaping ([String: AnyObject]) -> Void, failure:@escaping (Error) -> Void) {
        
        let url = baseUrl + urlLast
        
        Alamofire.request(url).responseJSON { (responseObject) -> Void in
            
//            print(responseObject)
            
            if responseObject.result.isSuccess {
                
                if responseObject.result.value != nil {
                    
//                    print(responseObject.result.value)
                    let resJson = responseObject.result.value! as? [String: AnyObject]
                    success(resJson!)
                }
                else {
                    
                    let dict = [String: AnyObject]()
                    success(dict)
                }
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    //Alamo get Array
    class func requestGETURLArry(_ urlLast: String, success:@escaping (Array<[String: AnyObject]>) -> Void, failure:@escaping (Error) -> Void) {
        
        let url = baseUrl + urlLast
        
        Alamofire.request(url).responseJSON { (responseObject) -> Void in
            
//            print(responseObject)
            
            if responseObject.result.isSuccess {
                
                if responseObject.result.value != nil {
                    
//                    print(responseObject.result.value)
                    if let resJson = responseObject.result.value! as? Array<[String: AnyObject]> {
                        
                        success(resJson)
                    }
                    else {
                        
                        let array = Array<[String: AnyObject]>()
                        success(array)
                    }
                }
                else {
                    
                    let array = Array<[String: AnyObject]>()
                    success(array)
                }
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    // alamo post
    class func postUrl(_ urlLast: String, parameters: Parameters, success:@escaping ([String: AnyObject]) -> Void, failure:@escaping (Error) -> Void) {
        
        let url = baseUrl + urlLast
        
        // for encoded data
        //        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
        //else
        //        Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { (responseObject) -> Void in
            
//            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = responseObject.result.value! as? [String: AnyObject]
                success(resJson!)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
}
