//
//  AllBrandViewController .swift
//  StylePlusproj
//
//  Created by WhiteOval mac on 10/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
import ObjectMapper
import Kingfisher
import ACProgressHUD_Swift

class AllBrandViewController: UIViewController {
    
    var brandCategoryData : CategoryData?
    var categoryProductDataArry : [CatSubs]?

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ACProgressHUD.shared.showHUD()
        getBrandCategoryData()
        let color = UIColor()
        self.navigationController?.navigationBar.backgroundColor = color.hexStringToUIColor(hex: "512DA8")
        self.navigationItem.title = self.brandCategoryData!.categoryName!
        
    }
            
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension AllBrandViewController {
    
    func getBrandCategoryData() {
        
        ApiCalls.getCategoryBrandData(categoryId:Int( brandCategoryData!.categoryId!)! , success: { (jsonArray) in
            
                self.categoryProductDataArry = Mapper<CatSubs>().mapArray(JSONArray: jsonArray)
                self.tableView.reloadData()
                ACProgressHUD.shared.hideHUD()
            
            }) { (error) in
                
                ACProgressHUD.shared.hideHUD()
                print(error.localizedDescription)
            }
        }
    
}

extension AllBrandViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.categoryProductDataArry?.count {
            
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllBrandTblVwCell", for: indexPath) as! AllBrandTblVwCell
        
        cell.allBrandimg?.kf.setImage(with: URL(string: self.categoryProductDataArry![indexPath.row].brandImage!))
        cell.allBrandLbl.text = self.categoryProductDataArry![indexPath.row].brandDescription!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let viewcontroller9 = storyboard?.instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
        
        viewcontroller9.fromAllBrand = true
        viewcontroller9.cBrandProductDataArry = self.categoryProductDataArry![indexPath.row]
        
        self.navigationController?.pushViewController(viewcontroller9, animated: true)
    }
}

