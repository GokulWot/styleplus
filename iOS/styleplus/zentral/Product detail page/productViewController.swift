//
//  productViewController.swift
//  StylePlusproj
//
//  Created by WhiteOval mac on 06/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit


extension UIImageView {
    func downloadedFrom(url3: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url3) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image2"),
                let data = data, error == nil,
                let image2 = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image2
            }
            }.resume()
    }
    func downloadedFrom(link3: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link3) else { return }
        downloadedFrom(url3: url, contentMode: mode)
    }
}


struct  Bproduct: Decodable

{
    let itemImage : String
    let itemName : String
    let brandId = Int()
    
    
    
}


class productViewController: UIViewController,UICollectionViewDataSource {
    @IBOutlet weak var itemnamelbl: UILabel!
    @IBOutlet weak var itemimage: UIImageView!
    @IBOutlet weak var itemdescription: UITextView!
    @IBOutlet weak var productcollectionview: UICollectionView!
   
    
    var bpro = [Bproduct] ()
   
    var  bid = Int()
    
    var bname = String()
    var btext = String()
   // var bimg =  String()
    //var nbid = Int ()
    override func viewDidLoad() {
        super.viewDidLoad()
    
    productcollectionview.dataSource = self
      
        itemnamelbl.text = self.bname
        itemdescription.text = self.btext
        
        
        
        
      // self.bid = allbra[IndexPath.row].brandId
        
        let z = URL(string:"http://34.238.104.190:8080/stylePlus/webService/brandProduct.jsp?mallId=66&brandId=\(bid)")
        
        
        //String(describing: self.Hbrands[IndexPath.row].brandId
        
        URLSession.shared.dataTask(with: z!) { (data, response, error) in
            
            if error == nil {
                
                do {
                    self.bpro = try JSONDecoder().decode([Bproduct].self, from: data!)
                 
                    
                }
                    
                catch {
                    print("Parse Error")
                    
                }
                
                DispatchQueue.main.async {
                    self.productcollectionview.reloadData()
                }
            }
            
            }.resume()
        
      
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        
      
            
            var reusable  : UICollectionReusableView? = nil
            if (kind == UICollectionElementKindSectionHeader)
            {
                
                let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "reusableview3", for: indexPath) as! reusableproductCollectionReusableView
                
                view.productlbl.text = "PRODUCTS"
                reusable = view
                return reusable!
            }
            
            return reusable!
        }
    
    
    
    
    
    
    
    
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return bpro.count
        }
        
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productcell", for: indexPath) as! productCollectionViewCell
            
            // cell.nameLbl.text = heroes[indexPath.row].expiryDate.capitalized
            
            if let imageURL = URL(string: bpro[indexPath.row].itemImage) {
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: imageURL)
                    if let data = data {
                        let itemImage = UIImage(data: data)
                        DispatchQueue.main.async {
                            cell.productimg?.image = itemImage
                            cell.itemnamelbl.text = self.bpro[indexPath.row].itemName
                            
                            
                            
                        }
                    }
                }
            }
            
            return cell
            
            
        }
        
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
