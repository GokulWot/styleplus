//
//  AccountDetailViewController.swift
//  zentral
//
//  Created by WhiteOval mac on 25/05/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
import ObjectMapper
import Kingfisher
import Alamofire
import ACProgressHUD_Swift

class AccountDetailViewController: UIViewController, UINavigationControllerDelegate, UITextFieldDelegate {

    var userId : Int?
    var userDataArray : [UserData]?
    var loyaltyDataArray : LoyaltyData?
    var fromAccount = true
    @IBOutlet weak var segmentController: TwicketSegmentedControl!
    @IBOutlet weak var firstview: UIView!
    @IBOutlet weak var secondview: UIView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var changePhtBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var photoTxt: UITextField!
    @IBOutlet weak var dobTxt: UITextField!
    @IBOutlet weak var genderTxt: UITextField!
    @IBOutlet weak var loyDescLbl: UILabel!
    @IBOutlet weak var loyPointLbl: UILabel!
    @IBOutlet weak var loyCardImg: UIImageView!
    @IBOutlet weak var loyCardLbl: UILabel!
    @IBOutlet weak var loyPurchLbl: UILabel!
    let thePicker = UIPickerView()
    let datePicker = UIDatePicker()
    let myPickerData = [String](arrayLiteral: "male", "female", "trans")
    var isEdit = false
    var imageClicked = false
    var parameter : Parameters?
    var loginData : LoginData?
    let imagePickerController = UIImagePickerController()
    let mallId = Constants.GlobalConst.mallId
    var takenimage : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        thePicker.delegate = self
        thePicker.dataSource = self
        ACProgressHUD.shared.showHUD()
        getLoyalityData()
        showDatePicker()
        // implimenting imagePicker
        imagePickerController.delegate = self
        
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard)) //hide keyboard on touching superview
        
        // to hide save button and display edit button
        self.saveBtn.isHidden = true
        self.editBtn.isHidden = false
        self.changePhtBtn.isHidden = true
        self.nameTxt.isUserInteractionEnabled = false
        self.emailTxt.isUserInteractionEnabled = false
        self.photoTxt.isUserInteractionEnabled = false
        self.dobTxt.isUserInteractionEnabled = false
        self.genderTxt.isUserInteractionEnabled = false
        
        let titles = ["Account", "Loyalty"]
        segmentController.setSegmentItems(titles)
        segmentController.delegate = self
        let color = UIColor()
        segmentController.defaultTextColor = UIColor.white
        segmentController.highlightTextColor = color.hexStringToUIColor(hex: "290E8F")
        segmentController.segmentsBackgroundColor = color.hexStringToUIColor(hex: "290E8F")
        segmentController.sliderBackgroundColor = UIColor.white
        if fromAccount {
            
            segmentController.move(to: 0)
            self.firstview.isHidden = false
            self.secondview.isHidden = true
        }
        else {
            
            segmentController.move(to: 1)
            self.firstview.isHidden = true
            self.secondview.isHidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if imageClicked == false {
        
            getUserData()
        }
    }
    
    @IBAction func backBtnPressed(_ sender: UIBarButtonItem) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuController")
        self.present(viewController!, animated: true, completion: nil)
    }
    
    @IBAction func rightSwipeDone(_ sender: Any) {
        
        segmentController.move(to: 0)
        self.firstview.isHidden = false
        self.secondview.isHidden = true
    }
    
    @IBAction func leftSwipeDone(_ sender: Any) {
        
        // to stop leftSwipe action while editing
        if !isEdit {
            segmentController.move(to: 1)
            self.firstview.isHidden = true
            self.secondview.isHidden = false
        }
       
    }
    
   // to edit account details
    @IBAction func editBtnPressed(_ sender: Any) {
        
        self.saveBtn.isHidden = false
        self.editBtn.isHidden = true
        self.changePhtBtn.isHidden = false
        self.nameTxt.isUserInteractionEnabled = true
        self.emailTxt.isUserInteractionEnabled = true
        self.photoTxt.isUserInteractionEnabled = true
        self.dobTxt.isUserInteractionEnabled = true
        self.genderTxt.isUserInteractionEnabled = true
        genderTxt.inputView = thePicker
        dobTxt.inputView = datePicker
        self.isEdit = true
        // to stop segmentControll while editing
        segmentController.isUserInteractionEnabled = false
    }
    
    @IBAction func saveBtnPressed(_ sender: Any) {
       
       // to  edit userData
        if(nameTxt.text == "" || emailTxt.text == "" || photoTxt.text == "" || dobTxt.text == ""){
            
            var myAlert = UIAlertController(title:"Alert",message:"All fields are required",preferredStyle:UIAlertController.Style.alert)
            let okaction = UIAlertAction(title:"OK",style:UIAlertAction.Style.default,handler:nil)
            myAlert.addAction(okaction)
            
            self.present(myAlert,animated:true,completion: nil)
        }
        else {
            
            func isValidEmail(emailID:String) -> Bool {
                let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
                let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
                return emailTest.evaluate(with: emailID)
            }
            
            
            if isValidEmail(emailID: emailTxt.text!) == false {
                
                var myAlert = UIAlertController(title:"Alert",message:"Please enter valid email address",preferredStyle:UIAlertController.Style.alert)
                let okaction = UIAlertAction(title:"OK",style:UIAlertAction.Style.default,handler:nil)
                myAlert.addAction(okaction)
                
                self.present(myAlert,animated:true,completion: nil)
            }
                
            else {
                
                func validate(value: String) -> Bool {
                    let PHONE_REGEX = "^[7-9][0-9]{9}$"
                    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
                    let result =  phoneTest.evaluate(with: value)
                    return result
                }
                
                if validate(value: photoTxt.text!) == false {
                    
                    let myAlert = UIAlertController(title:"Alert",message:"Please enter valid mobile number",preferredStyle:UIAlertController.Style.alert)
                    let okaction = UIAlertAction(title:"OK",style:UIAlertAction.Style.default,handler:nil)
                    myAlert.addAction(okaction)
                    
                    self.present(myAlert,animated:true,completion: nil)
                }
                    
                else {
                    
                    if self.profileImg.image == nil {
                        
                        let myAlert = UIAlertController(title:"Alert",message:"Please add an image",preferredStyle:UIAlertController.Style.alert)
                        let okaction = UIAlertAction(title:"OK",style:UIAlertAction.Style.default,handler:nil)
                        myAlert.addAction(okaction)
                        self.present(myAlert,animated:true,completion: nil)
                    }
                    else {
                        
                        uploadImage()
                    }
                }
            }
        }
    }
    
    @IBAction func changePhotoBtnPressed(_ sender: Any) {
       
        imageClicked = true
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = .photoLibrary
        present(controller, animated: true, completion: nil)
    }
}

private extension AccountDetailViewController {
    
    func getUserData() {
        
        userId = UserDefaults.standard.value(forKey: "userId") as? Int
        ApiCalls.getUserData(userId: userId!, success: { (jsonArray) in
            print(jsonArray)
            self.userDataArray = Mapper<UserData>().mapArray(JSONArray: jsonArray)
            ACProgressHUD.shared.hideHUD()
            do {
                
                let data = try Data(contentsOf: URL(string: (self.userDataArray![0].userImage)!)!)
                self.profileImg.image = UIImage(data: data)
            }
            catch {
                
                print("error catched")
            }
            self.nameTxt.text = self.userDataArray![0].name
            self.loyDescLbl.text = "Hi \(self.userDataArray![0].name!), congrats as you are one of our \(self.loyaltyDataArray!.cardType!) member. Take the privilege to attend special events and offers customized for you."
            self.emailTxt.text = self.userDataArray![0].email
            self.photoTxt.text = self.userDataArray![0].phone
            self.dobTxt.text = self.userDataArray![0].dob
            self.genderTxt.text = self.userDataArray![0].gender
            
        }) { (error) in
            
            print(error)
        }
    }
    
    func getLoyalityData() {
        
        userId = UserDefaults.standard.value(forKey: "userId") as? Int
        ApiCalls.getLoyalityData(userId: userId!, success: { (jsonDict) in
            
            self.loyaltyDataArray = Mapper<LoyaltyData>().map(JSONObject: jsonDict)
            self.loyCardLbl.text = self.loyaltyDataArray!.cardType!
            self.loyPointLbl.text = String(self.loyaltyDataArray!.points!)
            self.loyPurchLbl.text = String(self.loyaltyDataArray!.transaction!)
        }) { (error) in
            
            print(error)
        }
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //to add possible dates only
        let currentDate: Date = Date()
        self.datePicker.maximumDate = currentDate
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.bordered, target: self, action:  #selector(self.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.bordered, target: self, action:  #selector(self.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        dobTxt.inputAccessoryView = toolbar
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        dobTxt.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        
        self.view.endEditing(true)
    }
    
    @objc func dismissKeyboard() {
        // do aditional stuff
        view.endEditing(true)
    }
    
}


extension AccountDetailViewController: TwicketSegmentedControlDelegate {
    
    func didSelect(_ segmentIndex: Int) {
        
        if segmentIndex == 0 {
            
            self.firstview.isHidden = false
            self.secondview.isHidden = true
        }
        else {
            
            self.firstview.isHidden = true
            self.secondview.isHidden = false
        }
    }
}

extension AccountDetailViewController: UIPickerViewDataSource,UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return myPickerData.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return myPickerData[row]
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderTxt.text = myPickerData[row]
    }
}

extension AccountDetailViewController : UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        let image_data = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
        let imageData:Data = image_data!.jpegData(compressionQuality: 0.9)!
        print(imageData)
        self.profileImg.contentMode = .scaleAspectFill
        self.profileImg.image = image_data!
        self.profileImg.clipsToBounds = true
        dismiss(animated: true, completion: nil)
    }
}

extension AccountDetailViewController {
    
    func uploadImage() {
        
        ACProgressHUD.shared.showHUD()
        let userId = UserDefaults.standard.value(forKey:"userId") as? Int
        parameter = ["username": nameTxt.text!, "email":emailTxt.text!,"phoneNumber":photoTxt.text!,"dob":dobTxt.text!,"gender":genderTxt!.text,"userId":userId!]
        
        ApiCalls.editProfile(parameters: parameter!, success: { (jsonDict) in
//            print(jsonDict)
            self.loginData = Mapper<LoginData>().map(JSONObject: jsonDict)
//            print(self.loginData?.status)
            if self.loginData?.status == "success" {
                let imageData = (self.profileImg.image as! UIImage).jpegData(compressionQuality: 0.9)
//                print(imageData)
                let strBase64 = imageData!.base64EncodedString(options: .lineLength64Characters)
                self.takenimage = strBase64
                let baseUrl = Constants.GlobalConst.baseUrl
                
                let imgurlup = baseUrl+"/rest/AppUserImage/upload"
                
                Alamofire.request(imgurlup, method: .post, parameters: ["userId": userId!,"image":"\(self.takenimage!)"], headers: nil).responseString{
                    response in
                    switch response.result {
                    case .success(let data):
                        print(data)
                        if data == "true" {
                            
                            ACProgressHUD.shared.hideHUD()
                            let myAlert = UIAlertController(title:"Alert",message:"Data changed successfully",preferredStyle:UIAlertController.Style.alert)
                            let okaction = UIAlertAction(title:"OK",style:UIAlertAction.Style.default,handler: { _ -> Void in
                                
                                ACProgressHUD.shared.showHUD()
                                NotificationCenter.default.post(name: Notification.Name("SaveButtonPressed"), object: nil)
                                self.imageClicked = false
                                // to enable swipe action and segmentControll
                                self.saveBtn.isHidden = true
                                self.editBtn.isHidden = false
                                self.changePhtBtn.isHidden = true
                                self.nameTxt.isUserInteractionEnabled = false
                                self.emailTxt.isUserInteractionEnabled = false
                                self.photoTxt.isUserInteractionEnabled = false
                                self.dobTxt.isUserInteractionEnabled = false
                                self.genderTxt.isUserInteractionEnabled = false
                                self.segmentController.isUserInteractionEnabled = true
                                self.isEdit = false
                            })
                            myAlert.addAction(okaction)
                            self.present(myAlert,animated:true,completion: nil)
                        }
                        break
                    case .failure(let error):
                        
                        print(error)
                    }
                    
                    
                }
            }
        }) { (error) in
            
            print(error.localizedDescription)
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
