//
//  allofferViewController.swift
//  StylePlusproj
//
//  Created by WhiteOval mac on 08/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit



extension UIImageView {
    func downloadedFrom(url2: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url2) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image2"),
                let data = data, error == nil,
                let image2 = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image2
            }
            }.resume()
    }
    func downloadedFrom(link2: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link2) else { return }
        downloadedFrom(url2: url, contentMode: mode)
    }
}

struct Aoffer: Decodable {
    let offerImage: String
    let expiryDate: String
  let shopLogo: String
   let createdDate: String
   let offerDescription: String
    
}




class allofferViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {

    var alloff = [Aoffer] ()
    
    @IBOutlet weak var offcollectionview: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        offcollectionview.dataSource = self
        offcollectionview.delegate = self
        
        
        // Do any additional setup after loading the view.
        
        
        let x = URL(string: "http://34.238.104.190:8080/stylePlus/webService/allOffers.jsp?mallId=66&offset=1")
        URLSession.shared.dataTask(with: x!) { (data, response, error) in
            
            if error == nil {
                
                do {
                    self.alloff = try JSONDecoder().decode([Aoffer].self, from: data!)
                }
                catch {
                    print("Parse Error")
                    print (data)
                   // print (self.prod)
                }
                
                DispatchQueue.main.async {
                    self.offcollectionview.reloadData()
                }
            }
            
            }.resume()
        
        
    }
        
        
        
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return alloff.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "offerallcell", for: indexPath) as! allofferCollectionViewCell
        
        // cell.nameLbl.text = heroes[indexPath.row].expiryDate.capitalized
        
        if let imageURL = URL(string: alloff[indexPath.row].offerImage) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: imageURL)
                if let data = data {
                    let offerImage = UIImage(data: data)
                    DispatchQueue.main.async {
                        cell.alloffercellimg?.image = offerImage
                    }
                }
            }
        }
        
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let viewcontroller3 = storyboard?.instantiateViewController(withIdentifier: "detailofferViewController") as! detailofferViewController
        
        
        
        
        self.navigationController?.pushViewController(viewcontroller3, animated: true)
        
        if alloff[indexPath.row].shopLogo != nil{
            if let imageURL = URL(string: alloff[indexPath.row].offerImage) {
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: imageURL)
                    if let data = data {
                        let  offerImage = UIImage(data: data)
                        DispatchQueue.main.async {
                            
                            viewcontroller3.offerimage?.image =  offerImage
                            //viewcontroller.offerimage?.image = self.heroes[indexPath.row]
                            viewcontroller3.expdatelbl?.text = self.alloff[indexPath.row].expiryDate.capitalized
                            viewcontroller3.createddatelbl?.text = self.alloff[indexPath.row].createdDate.capitalized
                            
                            viewcontroller3.discripttextview?.text = self.alloff[indexPath.row].offerDescription
                            
                            
                        }
                        
                    }
                }
            }
            else{
                
                
                if  let imageURL = URL(string: alloff[indexPath.row].offerImage) {
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: imageURL)
                        if let data = data {
                            let offerImage = UIImage(data: data)
                            DispatchQueue.main.async {
                                
                                viewcontroller3.offerimage?.image = offerImage
                                //viewcontroller.offerimage?.image = self.heroes[indexPath.row]
                                
                                
                            }
                            
                            
                        }
                    }
                }
            }
            if let imageURL = URL(string: alloff[indexPath.row].shopLogo) {
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: imageURL)
                    if let data = data {
                        let shopLogo = UIImage(data: data)
                        DispatchQueue.main.async {
                            
                            viewcontroller3.shoplogoimg?.image = shopLogo
                            
                            
                        }
                        
                        
                    }
                }
            }
            
        }
        
        
        
        
        
        
        
        
        
        
    }
    
        
        
        
        
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
