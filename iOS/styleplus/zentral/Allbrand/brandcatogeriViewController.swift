//
//  brandcatogeriViewController.swift
//  StylePlusproj
//
//  Created by WhiteOval mac on 08/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
extension UIImageView {
    func downloadedFrom(url4: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url4) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image4"),
                let data = data, error == nil,
                let image4 = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image4
            }
            }.resume()
    }
    func downloadedFrom(link4: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link4) else { return }
        downloadedFrom(url3: url, contentMode: mode)
    }
}



struct Categories : Decodable{
    let categoryIcon : String
    let categoryName :String
   
    let categoryId : String
    
    
    
    
    
}


class brandcatogeriViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var catagoriescollectionview: UICollectionView!
    var catog = [Categories]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        catagoriescollectionview.dataSource = self
        catagoriescollectionview .delegate = self
        
        
        
        let d = URL(string: "http://34.238.104.190:8080/stylePlus/webService/categoryBrands.jsp?mallId=66")
        URLSession.shared.dataTask(with: d!) { (data, response, error) in
            
            if error == nil {
                
                do {
                    self.catog = try JSONDecoder().decode([Categories].self, from: data!)
                }
                catch {
                    print("Parse Error")
                    
                    
                }
                
                DispatchQueue.main.async {
                    self.catagoriescollectionview.reloadData()
                }
            }
            
            }.resume()
        
        
        
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return catog.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "catagcell", for: indexPath) as! catagoriesCollectionViewCell
        
        // cell.nameLbl.text = heroes[indexPath.row].expiryDate.capitalized
        
        if let imageURL = URL(string: catog[indexPath.row].categoryIcon) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: imageURL)
                if let data = data {
                    let categoryIcon = UIImage(data: data)
                    DispatchQueue.main.async {
                        cell.catimg?.image = categoryIcon
                        cell.catognamelbl?.text = self.catog[indexPath.row].categoryName
                    }
                }
            }
        }
        
        return cell
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let viewcontroller6 = storyboard?.instantiateViewController(withIdentifier: "allbrandViewController") as! allbrandViewController
        
        
        viewcontroller6.ctid = Int(catog[indexPath.row].categoryId)!
        
        self.navigationController?.pushViewController(viewcontroller6, animated: true)
       
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
