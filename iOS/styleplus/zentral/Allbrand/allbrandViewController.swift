//
//  allbrandViewController.swift
//  StylePlusproj
//
//  Created by WhiteOval mac on 10/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit


extension UIImageView {
    func downloadedFrom(url7: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url7) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image2"),
                let data = data, error == nil,
                let image2 = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image2
            }
            }.resume()
    }
    func downloadedFrom(link7: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link7) else { return }
        downloadedFrom(url7: url, contentMode: mode)
    }
}

struct  Abrand : Decodable {
    

 let  brandDescription : String
 let  brandImage : String
    
 let brandName : String
   
    let  brandId : Int
    
    
    
    
    
}

class allbrandViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate{

    
    var allbra = [Abrand]()
    
    var  ctid = Int()
   //  var items = [Hbrands]()
    
   
    
    @IBOutlet weak var allbrandcollectionview: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

      allbrandcollectionview.dataSource = self
       allbrandcollectionview.delegate = self
        
        // ADD CategoryId to GET VALUES FROM THIS URL
        let f = URL(string: "http://34.238.104.190:8080/stylePlus/webService/brandCategory.jsp?mallId=66&categoryId=\(ctid)")
        URLSession.shared.dataTask(with: f!) { (data, response, error) in
            
            if error == nil {
                
                do {
                    self.allbra = try JSONDecoder().decode([Abrand].self, from: data!)
                }
                catch {
                    print("Parse Error")
                    
                    
                }
                
                DispatchQueue.main.async {
                    self.allbrandcollectionview.reloadData()
                }
            }
            
            }.resume()
        
        
        
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allbra.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "allbrandcell", for: indexPath) as! allbrandCollectionViewCell
        
        // cell.nameLbl.text = heroes[indexPath.row].expiryDate.capitalized
        
        if let imageURL = URL(string: allbra[indexPath.row].brandImage) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: imageURL)
                if let data = data {
                    let brandImage = UIImage(data: data)
                    DispatchQueue.main.async {
                    cell.brandimgcell?.image = brandImage
                    cell.branddiscriptiontextvw?.text = self.allbra[indexPath.row].brandDescription
                        
                    }
                }
            }
        }
        
        return cell
        
      
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let viewcontroller9 = storyboard?.instantiateViewController(withIdentifier: "productViewController") as! productViewController
      
        
        
        viewcontroller9.bid  = allbra[indexPath.row].brandId
        
        
        
        
        
        
    viewcontroller9.bname = self.allbra[indexPath.row].brandName
    viewcontroller9.btext  = self.allbra[indexPath.row].brandDescription
   // viewcontroller8.bimg = self.allbra[indexPath.row].brandImage
        
        if  let imageURL = URL(string: allbra[indexPath.row].brandImage) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: imageURL)
                if let data = data {
                    let brandImage = UIImage(data: data)
                    DispatchQueue.main.async {
                        
                        viewcontroller9.itemimage?.image = brandImage
                        
                    
                    }
                }
            }
            
        }
        
        
        
        
                        
   // viewcontroller8.bimg = self.allbra[indexPath.row].brandImage
                 
        self.navigationController?.pushViewController(viewcontroller9, animated: true)
        
       
        
    }
    
    
            
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   



}

