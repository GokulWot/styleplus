//
//  AllOfferViewController.swift
//  StylePlusproj
//
//  Created by WhiteOval mac on 08/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
import ObjectMapper
import Kingfisher

class AllOfferViewController: UIViewController {
    
    var AllofferDataArry : [OfferData]?

    
    @IBOutlet weak var offerTblVw: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        sideMenuController!.isLeftViewSwipeGestureEnabled = false
        sideMenuController!.isRightViewSwipeGestureEnabled = false
        getOfferData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension AllOfferViewController {
    
    func getOfferData() {
        
        ApiCalls.getOffersData(success: { (jsonArray) in
            
            self.AllofferDataArry = Mapper<OfferData>().mapArray(JSONArray: jsonArray)
            print(self.AllofferDataArry!.count)
            self.offerTblVw.reloadData()
        }) { (error) in
            
            print(error.localizedDescription)
        }

    }
   
}

// MARK :- Tableview delegates
extension AllOfferViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let count = self.AllofferDataArry?.count {
            
            return count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllOffersTblVwCell", for: indexPath) as! AllOffersTblVwCell
        
        cell.offerImg.kf.setImage(with: URL(string: self.AllofferDataArry![indexPath.row].offerImage!))
        // print(self.AllofferDataArry![indexPath.row].offerImage!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let viewcontroller3 = storyboard?.instantiateViewController(withIdentifier: "DetailOfferViewController") as! DetailOfferViewController
        
        viewcontroller3.AllofferData = self.AllofferDataArry![indexPath.row]
        
        self.navigationController?.pushViewController(viewcontroller3, animated: true)
    }
}
