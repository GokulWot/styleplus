//
//  AppDelegate.swift
//  zentral
//
//  Created by WhiteOval mac on 04/05/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseInstanceID
import UserNotifications
import ACProgressHUD_Swift
import CoreLocation
import IQKeyboardManagerSwift
import Kingfisher

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    
    var window: UIWindow?
    
    var locationManager: CLLocationManager?
    
    let mallId = Constants.GlobalConst.mallId
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //progress hud
        // initializing progress hub
        ACProgressHUD.shared.progressText = "Loading"
        ACProgressHUD.shared.indicatorColor = .blue
        ACProgressHUD.shared.showHudAnimation = .shrinkIn
        ACProgressHUD.shared.dismissHudAnimation = .shrinkOut
        
        //keyboard manager
        IQKeyboardManager.shared.enable = true
        
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        
        //MARK:-Push Notifications
        if #available(iOS 10.0, *) {
            let authOptions : UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_,_ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
            //application.registerForRemoteNotifications()
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            //application.registerForRemoteNotifications()
        }
        application.registerForRemoteNotifications()
        // [END register_for_notifications]
        FirebaseApp.configure()
        hideTopNavSeprtn()
        geoLocationConfig()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        Messaging.messaging().shouldEstablishDirectChannel = false
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        ConnectToFCM()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
}

private extension AppDelegate {
    
    //fcm token generating
    func ConnectToFCM() {
        
        Messaging.messaging().shouldEstablishDirectChannel = true
        Messaging.messaging().subscribe(toTopic: mallId)
        if let token = InstanceID.instanceID().token() {
            print("DCS: " + token)
            
            UserDefaults.standard.set(token, forKey: "fcmid")
            UserDefaults.standard.synchronize()
        }
    }
    
    // MARK: hiding top bar seperation
    func hideTopNavSeprtn() {
        
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
    }
    
    // MARK: geofencing config
    func geoLocationConfig() {
        
        self.locationManager = CLLocationManager()
        self.locationManager!.delegate = self
        self.locationManager?.requestAlwaysAuthorization()
        self.locationManager?.startUpdatingHeading()
        
        // Your coordinates go here (lat, lon)
        let geofenceRegionCenter = CLLocationCoordinate2D(
            latitude: 8.5560595,
            longitude: 76.8818902
        )
        
        let geofenceRegion1 = CLCircularRegion(center: geofenceRegionCenter, radius: 100, identifier: "inMall")
        let geofenceRegion2 = CLCircularRegion(center: geofenceRegionCenter, radius: 300, identifier: "nearMall")
        let geofenceRegion3 = CLCircularRegion(center: geofenceRegionCenter, radius: 10000, identifier: "inCity")
        
        let geofenceRegionArry = [geofenceRegion1, geofenceRegion2,geofenceRegion3]
        
        for geofenceRegion in geofenceRegionArry {
            
            self.locationManager?.startMonitoring(for: geofenceRegion)
        }
    }
    
    //geofencing entry api calls
    func geoEntryHandle(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
        let userId = UserDefaults.standard.value(forKey: "userId") as! Int
        
        if region.identifier == "inMall" {
            
            UserDefaults.standard.set(true, forKey: "inMall")
            UserDefaults.standard.set(false, forKey: "nearMall")
            UserDefaults.standard.set(false, forKey: "inCity")
            ApiCalls.getFenceData(userId: userId, action: "enter", type: "location", location: "inmall")
        }
        else if region.identifier == "nearMall" {
            
            if let inMall = UserDefaults.standard.value(forKey: "inMall") as? Bool {
                
                if inMall == false {
                    
                    UserDefaults.standard.set(false, forKey: "inMall")
                    UserDefaults.standard.set(true, forKey: "nearMall")
                    UserDefaults.standard.set(false, forKey: "inCity")
                    ApiCalls.getFenceData(userId: userId, action: "enter", type: "location", location: "nearMall")
                }
            }
            else {
                
                UserDefaults.standard.set(false, forKey: "inMall")
                UserDefaults.standard.set(true, forKey: "nearMall")
                UserDefaults.standard.set(false, forKey: "inCity")
                ApiCalls.getFenceData(userId: userId, action: "enter", type: "location", location: "nearMall")
            }
        }
        else {
            
            if let inMall = UserDefaults.standard.value(forKey: "inMall") as? Bool {
                
                if inMall == false {
                    
                    if let nearMall = UserDefaults.standard.value(forKey: "nearMall") as? Bool {
                        print(nearMall)
                        if nearMall == false {
                            
                            UserDefaults.standard.set(false, forKey: "inMall")
                            UserDefaults.standard.set(false, forKey: "nearMall")
                            UserDefaults.standard.set(true, forKey: "inCity")
                            ApiCalls.getFenceData(userId: userId, action: "enter", type: "location", location: "inCity")
                        }
                    }
                    else {
                        
                        UserDefaults.standard.set(false, forKey: "inMall")
                        UserDefaults.standard.set(false, forKey: "nearMall")
                        UserDefaults.standard.set(true, forKey: "inCity")
                        ApiCalls.getFenceData(userId: userId, action: "enter", type: "location", location: "inCity")
                    }
                }
                else {
                    
                    UserDefaults.standard.set(false, forKey: "inMall")
                    UserDefaults.standard.set(false, forKey: "nearMall")
                    UserDefaults.standard.set(true, forKey: "inCity")
                    ApiCalls.getFenceData(userId: userId, action: "enter", type: "location", location: "inCity")
                }
            }
            else {
                
                if let nearMall = UserDefaults.standard.value(forKey: "nearMall") as? Bool {
                    print(nearMall)
                    if nearMall == false {
                        
                        UserDefaults.standard.set(false, forKey: "inMall")
                        UserDefaults.standard.set(false, forKey: "nearMall")
                        UserDefaults.standard.set(true, forKey: "inCity")
                        ApiCalls.getFenceData(userId: userId, action: "enter", type: "location", location: "inCity")
                    }
                }
                else {
                    
                    UserDefaults.standard.set(false, forKey: "inMall")
                    UserDefaults.standard.set(false, forKey: "nearMall")
                    UserDefaults.standard.set(true, forKey: "inCity")
                    ApiCalls.getFenceData(userId: userId, action: "enter", type: "location", location: "inCity")
                }
            }
        }
    }
    
    //geofencing exit api calls
    func geoExitHandle(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
        let userId = UserDefaults.standard.value(forKey: "userId") as! Int
        print(region.identifier)
        if region.identifier == "inMall" {
            
            UserDefaults.standard.set(false, forKey: "inMall")
            UserDefaults.standard.set(true, forKey: "nearMall")
            UserDefaults.standard.set(false, forKey: "inCity")
            ApiCalls.getFenceData(userId: userId, action: "exit", type: "location", location: "inMall")
            ApiCalls.getFenceData(userId: userId, action: "enter", type: "location", location: "nearMall")
        }
        else if region.identifier == "nearMall" {
            
            UserDefaults.standard.set(false, forKey: "inMall")
            UserDefaults.standard.set(false, forKey: "nearMall")
            UserDefaults.standard.set(true, forKey: "inCity")
            ApiCalls.getFenceData(userId: userId, action: "exit", type: "location", location: "nearMall")
            ApiCalls.getFenceData(userId: userId, action: "enter", type: "location", location: "inCity")
        }
        else {
            
            UserDefaults.standard.set(false, forKey: "inMall")
            UserDefaults.standard.set(false, forKey: "nearMall")
            UserDefaults.standard.set(false, forKey: "inCity")
            ApiCalls.getFenceData(userId: userId, action: "exit", type: "location", location: "nearMall")
        }
    }
}

// MARK: geofencing
extension AppDelegate: CLLocationManagerDelegate {
    
    // called when user Enters a monitored region
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
        print(region.identifier)
        
        if let isLogin : Bool = UserDefaults.standard.value(forKey: "isLoginKey") as? Bool {
            if (isLogin) {
                
                geoEntryHandle(manager, didEnterRegion: region)
            }
        }
    }
    
    // called when user Exits a monitored region
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
        print(region.identifier)
        
        if let isLogin : Bool = UserDefaults.standard.value(forKey: "isLoginKey") as? Bool {
            if (isLogin) {
                
                geoExitHandle(manager, didExitRegion: region)
            }
        }
    }
}

// MARK: firebase
extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        print("Firebase Registration Token \(fcmToken)")
        ConnectToFCM()
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
        print(fcmToken)
        ConnectToFCM()
    }
}

// MARK: Notification
@available(iOS 10.0, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        
        if let imageUrl = userInfo["imageUrl"] {
            print("Imageurl: \(imageUrl)")
            
            let alert = UIAlertController(title: "Offer", message:"", preferredStyle: UIAlertController.Style.alert)
            let url = URL(string: imageUrl as! String)
            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            let image = UIImage(data: data!)
            alert.addImage(image: image!)
            alert.addAction(UIAlertAction(title: "Open", style: UIAlertAction.Style.default, handler: { _ -> Void in
                
                if let offerId = userInfo["offerId"] {
                    print("Offer ID: \(offerId)")
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "DetailOfferViewController") as! DetailOfferViewController
                    profileViewController.fromView = "notif"
                    profileViewController.offerId = Int(offerId as! String)!
                    let nextNavi = UINavigationController(rootViewController: profileViewController)
                    nextNavi.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
                    self.window?.rootViewController = nextNavi
                }
            }))
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Decline", style: UIAlertAction.Style.cancel, handler: nil))
            
            // show alert
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindow.Level.alert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        Messaging.messaging().subscribe(toTopic: mallId)
        print("Notification did receive")
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let offerId = userInfo["offerId"] {
            print("Offer ID: \(offerId)")
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "DetailOfferViewController") as! DetailOfferViewController
            profileViewController.fromView = "notif"
            profileViewController.offerId = Int(offerId as! String)!
            let nextNavi = UINavigationController(rootViewController: profileViewController)
            nextNavi.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
            self.window?.rootViewController = nextNavi
        }
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
}

