//
//  ShopDetailsViewController.swift
//  zentral
//
//  Created by WhiteOval mac on 26/06/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
import AACarousel
import ObjectMapper
import Kingfisher
import ACProgressHUD_Swift

class ShopDetailsViewController: UIViewController{
    
    @IBOutlet weak var imageSlider: AACarousel!
    @IBOutlet weak var brandCollectionView: UICollectionView!
    @IBOutlet weak var productColectionView: UICollectionView!
    @IBOutlet weak var favBtn: UIBarButtonItem!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var colVwHeightConstr: NSLayoutConstraint!
    @IBOutlet weak var colVwInView: DesignableView!
    @IBOutlet weak var topBrandsVw: UIView!
    
    var subData : Subs?
    var favData : FavData?
    var shopDetailsDataArry : [ShopDetailsData]?
    var isfavBtnOnBool = false
    var fromSideMenu = false
    var fromFavMenu = false
    var shopname = String()
    var userId : Int?
    var shopId = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageSlider.delegate = self
        imageSlider.layerView.isHidden = true
       
        setPage()
        checkFav()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backBtnPressed(_ sender: UIBarButtonItem) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuController")
        self.present(viewController!, animated: true, completion: nil)
        
    }
    
    @IBAction func favBtnPressed(_ sender: UIBarButtonItem) {
        
        
        if let isLogin : Bool = UserDefaults .standard.value(forKey: "isLoginKey") as? Bool {
            
            if isLogin == false {
                
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavigationController")
                self.present(viewController!,  animated: true, completion: nil)
                
            }
                
            else {
                
                if isfavBtnOnBool {
                    
                    ACProgressHUD.shared.showHUD()
                    favBtn.image = #imageLiteral(resourceName: "favorite_unselected_white")
                    isfavBtnOnBool = false
                    NotificationCenter.default.post(name: Notification.Name("RightMenuBtnPressed"), object: nil)
                    removeFav()
                }
                    
                else {
                    
                    ACProgressHUD.shared.showHUD()
                    favBtn.image = #imageLiteral(resourceName: "favorite_selected_white")
                    isfavBtnOnBool = true
                    NotificationCenter.default.post(name: Notification.Name("RightMenuBtnPressed"), object: nil)
                    addFav()
                }
            }
        }
            
        else {
            
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavController")
            self.present(viewController!, animated: true, completion: nil)
        }
    }
}

// MARK:- Extension
extension ShopDetailsViewController {
    
    func getShopDetailsData() {
        
        ApiCalls.getShopDetailsData(shopId:shopId, success: { (jsonArray) in
            
            self.shopDetailsDataArry = Mapper<ShopDetailsData>().mapArray(JSONArray: jsonArray)
            
            if self.shopDetailsDataArry![0].itemdetails![0].status == "No item" {
                
                self.colVwInView.isHidden = true
            }
            else {
                
                if let count = self.shopDetailsDataArry![0].itemdetails?.count {
                    
                    if count % 2 == 0 {
                        
                        self.colVwInView.isHidden = false
                        let height = (self.view.frame.size.width/2) - 20
                        self.colVwHeightConstr.constant = CGFloat(Double(height) * Double(count / 2) * 1.05)
                        self.colVwInView.frame.size.height = self.productColectionView.frame.size.height
                    }
                    else {
                        
                        let count2 = count + 1
                        self.colVwInView.isHidden = false
                        let height = (self.view.frame.size.width/2) - 20
                        self.colVwHeightConstr.constant = CGFloat(Double(height) * Double(count2 / 2) * 1.05)
                        self.colVwInView.frame.size.height = self.productColectionView.frame.size.height
                    }
                    self.productColectionView.reloadData()
                }
            }
            if !self.fromFavMenu {
                
                self.descriptionLbl.text = self.shopDetailsDataArry![0].shopdetails![0].shopDescription!
            }
            self.setSliderImages()
            if self.shopDetailsDataArry![0].branddetails![0].status == "No item" {
                
                self.topBrandsVw.isHidden = false
            }
            else {
                
                self.brandCollectionView.reloadData()
            }
        }) { (error) in
            
            print(error)
        }
    }
    
    func setPage() {
        
        if fromFavMenu {
            
            shopId = favData!.favId!
            let color = UIColor()
            self.navigationItem.title = favData?.favName
            self.navigationController?.navigationBar.backgroundColor = color.hexStringToUIColor(hex: "512DA8")
            self.descriptionLbl.text = favData!.favDes!
            getShopDetailsData()
        }
        else {
            
            shopId = subData!.shopId!
            let color = UIColor()
            self.navigationItem.title = subData?.shopName
            self.navigationController?.navigationBar.backgroundColor = color.hexStringToUIColor(hex: "512DA8")
            getShopDetailsData()
            }
        }
    
    //checking if its favourite or not
    func checkFav() {
        
        if let isLogin : Bool = UserDefaults.standard.value(forKey: "isLoginKey") as? Bool {
            
            if isLogin == true {
                
                userId = UserDefaults.standard.value(forKey: "userId") as? Int
                
                ApiCalls.checkFav(toDo: "checkFavourites",brandId:shopId, userId: userId!, type: "shop", success: { (statusData) in
                    
                    let status = statusData["status"] as! String
                    if status == "success" {
                        
                        self.favBtn.image = #imageLiteral(resourceName: "favorite_selected_white")
                        self.isfavBtnOnBool = true
                    }
                    else {
                        
                        self.favBtn.image = #imageLiteral(resourceName: "favorite_unselected_white")
                        self.isfavBtnOnBool = false
                    }
                }) { (error) in
                    
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    //adding favourite
    func addFav() {
        
        ApiCalls.checkFav(toDo: "addFavourite",brandId:shopId, userId: userId!, type: "shop", success: { (statusData) in
            
            let status = statusData["status"] as! String
//            print(status)
            if status == "success" {
                
            }
            else {
                
                ACProgressHUD.shared.hideHUD()
            }
        }) { (error) in
            
            ACProgressHUD.shared.hideHUD()
            print(error.localizedDescription)
        }
    }
    
    //removing favourite
    func removeFav() {
        
        ApiCalls.checkFav(toDo: "unFavourites",brandId:shopId, userId: userId!, type: "shop", success: { (statusData) in
            
            let status = statusData["status"] as! String
            //            print(status)
            if status == "success" {
                
            }
            else {
                
                ACProgressHUD.shared.hideHUD()
            }
        }) { (error) in
            
            ACProgressHUD.shared.hideHUD()
            print(error.localizedDescription)
        }
    }
    
    
    //image string from offer data is appended to an array
    func setSliderImages() {
        
        var imageUrlStringArry = [String]()
        let offerDataArray: [Offerdetails] = shopDetailsDataArry![0].offerdetails!
        
        for item in offerDataArray {
            
//            print(item.offerImage!)
            if item.status == "Success" {
                
                imageUrlStringArry.append(item.offerImage!)
            }
                
            else {
                
                imageSlider.isHidden = true
            }
        }
        print(imageUrlStringArry.count)
        imageSlider.setCarouselData(paths: imageUrlStringArry, describedTitle: [""], isAutoScroll: true, timer: 5.0, defaultImage: "placeholder")
    }
}

// MARK:- collection view delegates
extension ShopDetailsViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.productColectionView {
            
            let width = (self.view.frame.size.width/2) - 40//some width
            let height = (self.view.frame.size.width/2) - 20
            return CGSize(width: width, height: height)
        }
        
        return CGSize(width: 100, height: 100)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        
        if collectionView == self.brandCollectionView{
            
            if let count = shopDetailsDataArry?.count {
                
                return count
            }
           
            return 0
            
           }
           
        else {
        
            if let count = shopDetailsDataArry?.count {
                
                return count
            }
        
        return 0
        
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.brandCollectionView {
            
            if let count = shopDetailsDataArry![section].branddetails?.count{
                
                return count
            }
            
            return 0
        }
            
        else {
        
            if let count = shopDetailsDataArry![section].itemdetails?.count {
                
                return count
            }
        
            return 0
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.brandCollectionView{
            
            let viewController = storyboard?.instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
            
            viewController.fromShopBrand = true
            viewController.shopDetailsData =  self.shopDetailsDataArry![0].branddetails?[indexPath.row]
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.brandCollectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeBrandColVwCell", for: indexPath) as! HomeBrandColVwCell
            if self.shopDetailsDataArry![0].branddetails![indexPath.item].status == "Success" {
                
                cell.brandofferimg.kf.setImage(with:URL(string: self.shopDetailsDataArry![0].branddetails![indexPath.item].brand_image!))
            }
                
            else {
                
                
            }
            
            return cell
        }
        else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductColVwCell", for: indexPath) as! ProductColVwCell
            
            if self.shopDetailsDataArry![0].itemdetails![indexPath.item].status == "Success" {
                
                cell.itemofrimg.kf.setImage(with:URL(string:self.shopDetailsDataArry![0].itemdetails![indexPath.item].item_image!))
            }
            else {
                
                
                
            }
            return cell
        }
    }
}

// MARK: image slider delegates
extension ShopDetailsViewController: AACarouselDelegate {
    
    //require method
    func downloadImages(_ url: String, _ index:Int) {
        
        let imageView = UIImageView()
        imageView.kf.setImage(with: URL(string: url)!, placeholder: UIImage.init(named: "placeholder"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { (downloadImage, error, cacheType, url) in
            self.imageSlider.images[index] = downloadImage!
        })
    }
    
    // didSecelct of CarouselView
    func didSelectCarouselView(_ view: AACarousel, _ index: Int) {
        
        
        let viewcontroller = storyboard?.instantiateViewController(withIdentifier: "DetailOfferViewController") as! DetailOfferViewController
        
        viewcontroller.offerDetailsData =  self.shopDetailsDataArry![0].offerdetails?[index]
        viewcontroller.fromView = "shopdeatils"
        
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }

    func callBackFirstDisplayView(_ imageView: UIImageView, _ url: [String], _ index: Int) {
        
        imageView.kf.setImage(with: URL(string: url[index]), placeholder: UIImage.init(named: "placeholder"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
    }
}

