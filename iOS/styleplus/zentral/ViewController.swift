//
//  ViewController.swift
//  zentral
//
//  Created by WhiteOval mac on 04/05/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit 
import AACarousel
import ObjectMapper
import Kingfisher
import ACProgressHUD_Swift
import LGSideMenuController

class ViewController: UIViewController {
   
    var hotDealDataArry : [HotDealsData]?
    var offerDataArry : [OfferData]?
    var brandDataArry : [BrandData]?
    let color = UIColor()
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageSlider: AACarousel!
    @IBOutlet weak var tableInView: DesignableView!
    @IBOutlet weak var viewInScrollHeightContr: NSLayoutConstraint!
    @IBOutlet weak var topBrandVw: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        ACProgressHUD.shared.showHUD()
        sideMenuController!.isLeftViewSwipeGestureEnabled = true
        sideMenuController!.isRightViewSwipeGestureEnabled = true
        imageSlider.delegate = self
        imageSlider.layerView.isHidden = true
        self.getBrandData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        ACProgressHUD.shared.showHUD()
        sideMenuController!.isLeftViewSwipeGestureEnabled = true
        sideMenuController!.isRightViewSwipeGestureEnabled = true
        
        self.getHotDealsData()
        self.getOfferData()
    }
 
    @IBAction func lmenubttn(_ sender: UIBarButtonItem) {
        
        toggleLeftView(nil)
    }
    
    @IBAction func rmenubttn(_ sender: UIBarButtonItem) {
        
        toggleRightView(nil)
    }

}

private extension ViewController {
    
    //get hot deals data from api
    func getHotDealsData() {
        
        ApiCalls.getHotDealsData(success: { (jsonArray) in
            
            self.hotDealDataArry = Mapper<HotDealsData>().mapArray(JSONArray: jsonArray)
//            print(self.hotDealDataArry?.count)
            if self.hotDealDataArry?.count == 0 {
                
                self.imageSlider.isHidden = true
            }
            else {
                
                self.setSliderImages()
            }
        }) { (error) in
            
            print(error.localizedDescription)
            self.imageSlider.isHidden = true
        }
    }
    
    //image string from offer data is appended to an array
    func setSliderImages() {
        
        var imageUrlStringArry = [String]()
        
        for item in self.hotDealDataArry! {
            
            //            print(item.offerImage!)
            imageUrlStringArry.append(item.offerImage!)
        }
        //        print(imageUrlStringArry.count)
        imageSlider.setCarouselData(paths: imageUrlStringArry, describedTitle: [""], isAutoScroll: true, timer: 5.0, defaultImage: "placeholder")
    }
    
    //get hot deals data from api
    func getOfferData() {
        
        ApiCalls.getOffersData(success: { (jsonArray) in
            
            self.offerDataArry = Mapper<OfferData>().mapArray(JSONArray: jsonArray)
//            print(self.offerDataArry?.count)
            
            if self.offerDataArry?.count == 0 {
                
                ACProgressHUD.shared.hideHUD()
                self.tableInView.isHidden = true
            }
            else {
                
                self.tableInView.isHidden = false
                DispatchQueue.main.async {
                    self.tableView.frame.size.height = CGFloat(200 * self.offerDataArry!.count)
                    self.tableInView.frame.size.height = 50.0 + self.tableView.frame.size.height
                    self.viewInScrollHeightContr.constant = 200.0 + self.tableInView.frame.size.height
                    self.tableView.reloadData()
                }
            }
        }) { (error) in
            
            ACProgressHUD.shared.hideHUD()
            print(error.localizedDescription)
            self.tableInView.isHidden = true
        }
    }
    
    //get brand data from api
    func getBrandData() {
        
        ApiCalls.getBrandData(success: { (jsonArray) in
            
            self.brandDataArry = Mapper<BrandData>().mapArray(JSONArray: jsonArray)
//            print(self.brandDataArry!.count)
            if self.brandDataArry!.count == 0 {
                
                ACProgressHUD.shared.hideHUD()
                self.topBrandVw.removeFromSuperview()
            }
            else {
                
                DispatchQueue.main.async {
                    
                    self.collectionView.reloadData()
                }
            }
        }) { (error) in
            
            ACProgressHUD.shared.hideHUD()
            print(error.localizedDescription)
        }
    }
}

// MARK: collection view delegates
extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let count = self.brandDataArry?.count {
            
            return count
        }
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BrandColVwCell", for: indexPath) as! BrandColVwCell
        
        cell.brandImg.kf.setImage(with: URL(string: self.brandDataArry![indexPath.row].brandImage!))
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
        
        viewController.brandData = self.brandDataArry![indexPath.row]
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

// MARK: table view delegates
extension ViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let count = self.offerDataArry?.count {
            
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OfferTabVwCell", for: indexPath) as! OfferTabVwCell
        
        ACProgressHUD.shared.hideHUD()
        cell.offerImg.kf.setImage(with: URL(string: self.offerDataArry![indexPath.row].offerImage!))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "DetailOfferViewController") as! DetailOfferViewController
        
        viewController.fromView = "offer"
        viewController.offerData = self.offerDataArry![indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

// MARK: image slider delegates
extension ViewController : AACarouselDelegate {
    
    //require method
    func downloadImages(_ url: String, _ index:Int) {
        
        let imageView = UIImageView()
        imageView.kf.setImage(with: URL(string: url)!, placeholder: UIImage.init(named: "placeholder"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: { (downloadImage, error, cacheType, url) in
            self.imageSlider.images[index] = downloadImage!
        })
    }
    
    func didSelectCarouselView(_ view: AACarousel, _ index: Int) {
        
        let viewController = storyboard?.instantiateViewController(withIdentifier: "DetailOfferViewController") as! DetailOfferViewController
        
        viewController.hotDealData = self.hotDealDataArry![index]
        viewController.fromView = "hotDeals"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func callBackFirstDisplayView(_ imageView: UIImageView, _ url: [String], _ index: Int) {
        
        imageView.kf.setImage(with: URL(string: url[index]), placeholder: UIImage.init(named: "placeholder"), options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
    }
}
