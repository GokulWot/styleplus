//
//  LoginViewController.swift
//  STYLEPLUSPROJECT
//
//  Created by WhiteOval mac on 17/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import ACProgressHUD_Swift

class LoginViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var mobileNumberTxt: UITextField!
    
    var parameter : Parameters?
    var loginData : LoginData?
    let mallId = Constants.GlobalConst.mallId
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard)) //hide keyboard on touching superview
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func dismissKeyboard() {
        // do aditional stuff
        view.endEditing(true)
    }
    
    @IBAction func loginbtn(_ sender: UIButton) {
        
        loginUser()
    }
    
    @IBAction func signInBtnPressed(_ sender: Any) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.present(viewController, animated: true, completion: nil)
    }
    @IBAction func backBtnPressed(_ sender: Any) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuController")
        self.present(viewController!, animated: true, completion: nil)
    }
}

//MARK:- Extension
private extension LoginViewController {
    
    func loginUser() {
        
        if(mobileNumberTxt.text! == ""){
            
            let myAlert = UIAlertController(title:"Alert",message:"LoginFailed",preferredStyle:UIAlertController.Style.alert)
            let okaction = UIAlertAction(title:"OK",style:UIAlertAction.Style.default,handler:nil)
            myAlert.addAction(okaction)
            
            self.present(myAlert,animated:true,completion: nil)
        }
        else {
            
            func validate(value: String) -> Bool {
                let PHONE_REGEX = "^[7-9][0-9]{9}$"
                let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
                let result =  phoneTest.evaluate(with: value)
                return result
            }
            
            if validate(value: mobileNumberTxt.text!) == false {
                //presentlbl.isHidden = false
                //presentlbl.text = "Please enter valid mobilenumber"
                
                
                let myAlert = UIAlertController(title:"Alert",message:"Please enter valid mobile number",preferredStyle:UIAlertController.Style.alert)
                let okaction = UIAlertAction(title:"OK",style:UIAlertAction.Style.default,handler:nil)
                myAlert.addAction(okaction)
                self.present(myAlert,animated:true,completion: nil)
            }
                
            else {
                
                ACProgressHUD.shared.showHUD()
                let fcmId = UserDefaults.standard.value(forKey:"fcmid") as? String
                parameter = ["username": mobileNumberTxt.text!, "mallId": mallId, "FCMId": fcmId!]
                
                ApiCalls.logIn(parameters: parameter!, success: { (jsonDict) in
                    
                    self.loginData = Mapper<LoginData>().map(JSONObject: jsonDict)
                    if self.loginData?.status == "success" {
                        
                        ACProgressHUD.shared.hideHUD()
                        UserDefaults.standard.setValue(true, forKey: "isLoginKey")
                        UserDefaults.standard.set(self.loginData?.userId, forKey: "userId")
                        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuController")
                        self.present(viewController!, animated: true, completion: nil)
                    }
                    else {
                        
                        ACProgressHUD.shared.hideHUD()
                        let myAlert = UIAlertController(title:"Alert",message:"Please enter correct mobile number or sign up for one",preferredStyle:UIAlertController.Style.alert)
                        let okaction = UIAlertAction(title:"OK",style:UIAlertAction.Style.default,handler:nil)
                        myAlert.addAction(okaction)
                        self.present(myAlert,animated:true,completion: nil)
                    }
                }) { (error) in
                    
                    ACProgressHUD.shared.hideHUD()
                    print(error.localizedDescription)
                }
            }
        }
    }
}


