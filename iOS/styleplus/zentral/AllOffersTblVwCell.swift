//
//  AllOffersTblVwCell.swift
//  zentral
//
//  Created by WhiteOval mac on 31/07/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit

class AllOffersTblVwCell: UITableViewCell {

    @IBOutlet weak var offerImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
