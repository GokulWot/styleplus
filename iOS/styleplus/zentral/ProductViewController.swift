//
//  ProductViewController.swift
//  StylePlusproj
//
//  Created by WhiteOval mac on 06/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
import ObjectMapper
import ACProgressHUD_Swift

class ProductViewController: UIViewController {
    
    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var itemDescriptionLbl: UILabel!
    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var favBtn: UIBarButtonItem!
    @IBOutlet weak var colVwInView: DesignableView!
    @IBOutlet weak var colVwHeightConstr: NSLayoutConstraint!
    
    var brandProductDataArry : [BrandProductData]?
    var brandData : BrandData?
    var favData : FavData?
    var cBrandProductDataArry : CatSubs?
    var shopDetailsData : Branddetails?
    var isfavBtnOnBool = false
    var fromSideMenu = false
    var fromAllBrand = false
    var fromShopBrand = false
    var brandId = Int()
    var userId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenuController!.isLeftViewSwipeGestureEnabled = false
        sideMenuController!.isRightViewSwipeGestureEnabled = false
        
        setUpPage()
        
        getBrandProductData()
        
        checkFav()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backBtnPressed(_ sender: Any) {
        
        if fromSideMenu {
            
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuController")
            self.present(viewController!, animated: true, completion: nil)
        }
        else {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func favBtnPressed(_ sender: Any) {
        
        if let isLogin : Bool = UserDefaults.standard.value(forKey: "isLoginKey") as? Bool {
            
            if isLogin == false {
                
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavController")
                self.present(viewController!, animated: true, completion: nil)
            }
            else {
                
                if isfavBtnOnBool {
                    
                    ACProgressHUD.shared.showHUD()
                    self.favBtn.image = #imageLiteral(resourceName: "favorite_unselected_white")
                    self.isfavBtnOnBool = false
                    NotificationCenter.default.post(name: Notification.Name("RightMenuBtnPressed"), object: nil)
                    removeFav()
                }
                else {
                    
                    ACProgressHUD.shared.showHUD()
                    self.favBtn.image = #imageLiteral(resourceName: "favorite_selected_white")
                    self.isfavBtnOnBool = true
                    NotificationCenter.default.post(name: Notification.Name("RightMenuBtnPressed"), object: nil)
                    addFav()
                }
            }
        }
        else {
            
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavController")
            self.present(viewController!, animated: true, completion: nil)
        }
    }
}

//MARK:- Extension
private extension ProductViewController {
    
    //get brand product data from api
    func getBrandProductData() {
        
        ApiCalls.getBrandProductData(brandId: brandId, success: { (jsonArray) in
            
            self.brandProductDataArry = Mapper<BrandProductData>().mapArray(JSONArray: jsonArray)
//            print(self.brandProductDataArry!.count)
            
            if self.brandProductDataArry![0].status == "No Result" {
                
                self.colVwInView.isHidden = true
            }
            else {
                
                if let count = self.brandProductDataArry?.count {
                    
                    if count % 2 == 0 {
                        
                        let height = (self.view.frame.size.width/2) + 20
                        self.colVwHeightConstr.constant = CGFloat(Double(height) * Double(count / 2))
                        self.colVwInView.frame.size.height = self.productCollectionView.frame.size.height
                    }
                    else {
                        
                        let count2 = count + 1
                        let height = (self.view.frame.size.width/2) + 20
                        self.colVwHeightConstr.constant = CGFloat(Double(height) * Double(count2 / 2))
                        self.colVwInView.frame.size.height = self.productCollectionView.frame.size.height
                    }
                    self.productCollectionView.reloadData()
                }
            }
                
        }) { (error) in
            
            print(error.localizedDescription)
        }
    }
    
    func setUpPage() {
        
        if fromSideMenu {
            
            brandId = favData!.favId!
            let color = UIColor()
            self.navigationController?.navigationBar.backgroundColor = color.hexStringToUIColor(hex: "290E8F")
            self.navigationItem.title = self.favData?.favName
            itemImg.kf.setImage(with: URL(string: (self.favData?.favLogo)!))
            itemDescriptionLbl?.text = self.favData?.favDes
        }
        else if fromAllBrand {
            
            brandId = cBrandProductDataArry!.brandId!
            let color = UIColor()
            self.navigationController?.navigationBar.backgroundColor = color.hexStringToUIColor(hex: "290E8F")
            self.navigationItem.title = self.cBrandProductDataArry!.brandName
            itemImg.kf.setImage(with: URL(string: (self.cBrandProductDataArry!.brandImage)!))
            itemDescriptionLbl?.text = self.cBrandProductDataArry!.brandDescription!
            
            
        }
        else if fromShopBrand {
            
            brandId =  Int((shopDetailsData?.brand_id!)!)!
            let color = UIColor()
            self.navigationController?.navigationBar.backgroundColor = color.hexStringToUIColor(hex: "290E8F")
            self.navigationItem.title = self.shopDetailsData!.brand_name
            itemImg.kf.setImage(with: URL(string: (self.shopDetailsData!.brand_image)!))
            itemDescriptionLbl?.text = self.shopDetailsData!.brand_description!
        }
        else {
            
            brandId = brandData!.brandId!
            self.navigationItem.title = self.brandData?.brandName
            itemImg.kf.setImage(with: URL(string: (self.brandData?.brandImage)!))
            itemDescriptionLbl?.text = self.brandData?.brandDescription
        }
    }
    
    //checking if its favourite or not
    func checkFav() {
        
        if let isLogin : Bool = UserDefaults.standard.value(forKey: "isLoginKey") as? Bool {
            
            if isLogin == true {
                
                userId = UserDefaults.standard.value(forKey: "userId") as? Int
                
                ApiCalls.checkFav(toDo: "checkFavourites",brandId: brandId, userId: userId!, type: "brand", success: { (statusData) in
                    
                    let status = statusData["status"] as! String
                    if status == "success" {
                        
                        self.favBtn.image = #imageLiteral(resourceName: "favorite_selected_white")
                        self.isfavBtnOnBool = true
                    }
                    else {
                        
                        self.favBtn.image = #imageLiteral(resourceName: "favorite_unselected_white")
                        self.isfavBtnOnBool = false
                    }
                }) { (error) in
                    
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func addFav() {
        
        ApiCalls.checkFav(toDo: "addFavourite",brandId: brandId, userId: userId!, type: "brand", success: { (statusData) in
            
            let status = statusData["status"] as! String
//            print(status)
            if status == "success" {
                
            }
            else {
                
                ACProgressHUD.shared.hideHUD()
            }
        }) { (error) in
            
            ACProgressHUD.shared.hideHUD()
            print(error.localizedDescription)
        }
    }
    
    func removeFav() {
        
        ApiCalls.checkFav(toDo: "unFavourites",brandId: brandId, userId: userId!, type: "brand", success: { (statusData) in
            
            let status = statusData["status"] as! String
//            print(status)
            if status == "success" {
                
            }
            else {
                
                ACProgressHUD.shared.hideHUD()
            }
        }) { (error) in
            
            ACProgressHUD.shared.hideHUD()
            print(error.localizedDescription)
        }
    }
}

//MARK Tableview delegates
extension ProductViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.size.width/2) - 20 //some width
        let height = (self.view.frame.size.width/2) + 20
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let count = self.brandProductDataArry?.count {
            
            return count
        }
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell
        
        cell.productimg?.kf.setImage(with: URL(string: self.brandProductDataArry![indexPath.row].itemImage!))
        cell.itemnamelbl.text = self.brandProductDataArry![indexPath.row].itemName
        
        return cell
    }
}
