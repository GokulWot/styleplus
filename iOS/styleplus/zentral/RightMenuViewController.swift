//
//  RightMenuViewController.swift
//  STYLEPLUSPROJECT
//
//  Created by WhiteOval mac on 12/04/18.
//  Copyright © 2018 WhiteOval mac. All rights reserved.
//

import UIKit
import ObjectMapper
import ACProgressHUD_Swift

class RightMenuViewController: UIViewController {
    
    var userId : Int?
    var userDataArray : [UserData]?
    var favDataArray : [FavData]?
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var favTabVw: UITableView!
    @IBOutlet weak var favLbl: UILabel!
    var type : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationRight(notification:)), name: Notification.Name("RightMenuBtnPressed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationSaved(notification:)), name: Notification.Name("SaveButtonPressed"), object: nil)
        setUpPage()
        if let isLogin : Bool = UserDefaults.standard.value(forKey: "isLoginKey") as? Bool {
            if (isLogin) {
                
                DispatchQueue.main.async {
                    
                    self.getUserData()
                    self.getFavList()
                }
            }
            else {
                
                ACProgressHUD.shared.hideHUD()
            }
        }
        else {
            
            ACProgressHUD.shared.hideHUD()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        if let isLogin : Bool = UserDefaults.standard.value(forKey: "isLoginKey") as? Bool {
//            if (isLogin) {
//                
//                getUserData()
//                getFavList()
//                
//            }
//        }
    }
    
    @objc func methodOfReceivedNotificationRight(notification: Notification){
        
        setUpPage()
        if let isLogin : Bool = UserDefaults.standard.value(forKey: "isLoginKey") as? Bool {
            if (isLogin) {
                
                DispatchQueue.main.async {
                    
                    self.getFavList()
                }
            }
            else {
                
                ACProgressHUD.shared.hideHUD()
            }
        }
        else {
            
            ACProgressHUD.shared.hideHUD()
        }
    }
    
    @objc func methodOfReceivedNotificationSaved(notification: Notification){
        
        setUpPage()
        if let isLogin : Bool = UserDefaults.standard.value(forKey: "isLoginKey") as? Bool {
            if (isLogin) {
                
                DispatchQueue.main.async {
                    
                    self.getUserData()
                }
            }
            else {
                
                ACProgressHUD.shared.hideHUD()
            }
        }
        else {
            
            ACProgressHUD.shared.hideHUD()
        }
    }
    
    @IBAction func accountBtnPressed(_ sender: Any) {
        
        if let isLogin : Bool = UserDefaults.standard.value(forKey: "isLoginKey") as? Bool {
            
            if isLogin == false {
                
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavController")
                self.present(viewController!, animated: true, completion: nil)
                
            }
            else {
                
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AccountDetailViewController") as! AccountDetailViewController
                
                nextViewController.fromAccount = true
                let nextNavi = UINavigationController(rootViewController: nextViewController)
                nextNavi.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
                
                self.present(nextNavi, animated: true, completion: nil)
            }
            
        }
            
        else {
            
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavController")
            self.present(viewController!, animated: true, completion: nil)
        }
    }
    
    @IBAction func loyalityBtnPressed(_ sender: Any) {
        
        if let isLogin : Bool = UserDefaults.standard.value(forKey: "isLoginKey") as? Bool {
            
            if isLogin == false {
                
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavController")
                self.present(viewController!, animated: true, completion: nil)
            }
            else {
                
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AccountDetailViewController") as! AccountDetailViewController
                
                nextViewController.fromAccount = false
                let nextNavi = UINavigationController(rootViewController: nextViewController)
                nextNavi.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
                
                self.present(nextNavi, animated: true, completion: nil)
            }
        }
        else {
            
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavController")
            self.present(viewController!, animated: true, completion: nil)
        }
    }
    
}

//MARK:- Extension
private extension RightMenuViewController {
    
    func setUpPage() {
        
        if let isLogin : Bool = UserDefaults.standard.value(forKey: "isLoginKey") as? Bool {
            
            if isLogin == false {
                
                self.favLbl.isHidden = true
                self.favTabVw.isHidden = true
            }
            else {
                
                userId = UserDefaults.standard.value(forKey: "userId") as? Int
                self.favLbl.isHidden = false
                self.favTabVw.isHidden = false
            }
        }
        else {
            
            self.favLbl.isHidden = true
            self.favTabVw.isHidden = true
        }
    }
    
    func getUserData() {
        
        ApiCalls.getUserData(userId: userId!, success: { (jsonArray) in
            
            self.userDataArray = Mapper<UserData>().mapArray(JSONArray: jsonArray)
            do {
                
                let data = try Data(contentsOf: URL(string: (self.userDataArray![0].userImage)!)!)
                self.userImg.image = UIImage(data: data)
            }
            catch {
                
                print("error catched")
            }
            ACProgressHUD.shared.hideHUD()
            self.userNameLbl.text = self.userDataArray![0].name
            
        }) { (error) in
            
            print(error)
        }
    }
    
    func getFavList() {
        
        ApiCalls.getFavData(userId: userId!, success: { (jsonArray) in
            
            self.favDataArray = Mapper<FavData>().mapArray(JSONArray: jsonArray)
            if self.favDataArray?.count == 0 {
                
                self.favLbl.isHidden = true
                self.favTabVw.isHidden = true
            }
            else {
                
                DispatchQueue.main.async {
                    
                    self.favTabVw.reloadData()
                }
            }
        }) { (error) in
            
            print(error)
        }
    }
}

//MARK:- Tableview delegates
extension RightMenuViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let count = self.favDataArray?.count {
            
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavTblVwCell") as! FavTblVwCell
        
        ACProgressHUD.shared.hideHUD()
        cell.favNameLbl.text = self.favDataArray![indexPath.row].favName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let type = self.favDataArray![indexPath.row].type
        
        if type == "brand"{
            
            
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
            
            nextViewController.favData = favDataArray![indexPath.row]
            nextViewController.fromSideMenu = true
            
            let nextNavi = UINavigationController(rootViewController: nextViewController)
            nextNavi.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
            self.present(nextNavi, animated: true, completion: nil)
        }
            
        else {
            
            let nextViewController2 = self.storyboard?.instantiateViewController(withIdentifier: "ShopDetailsViewController") as! ShopDetailsViewController
            
            nextViewController2.favData = favDataArray![indexPath.row]
            nextViewController2.fromFavMenu = true
            
            let nextNavi = UINavigationController(rootViewController: nextViewController2)
            nextNavi.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
            self.present(nextNavi, animated: true, completion: nil)
            
        }
    }
}
