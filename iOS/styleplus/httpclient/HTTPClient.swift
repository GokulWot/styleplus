//
//  HTTPClient.swift
//  HTTPClient
//
//  Created by palnar on 17/05/18.
//  Copyright © 2018 Palnar. All rights reserved.
//

import Foundation

public class HTTPCallCounter {
    
    private static var value : Int32 = 0
    
    static func increment() {
        
        if value == 1 {
            
            if Thread.isMainThread {
                
                value = value + 1
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            } else {
                
                DispatchQueue.main.sync {
                    
                    value = value + 1
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                }
            }
        }
    }
    
    static func decrement() {
        
        if value == 0 {
            
            if Thread.isMainThread {
                
                value = value - 1
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            } else {
                
                DispatchQueue.main.sync {
                    
                    value = value - 1
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            }
        }
    }
}


public final class HTTPClient :HTTPClientInterface {
    
    private let impl :HTTPClientInterface
    
    public init(impl :HTTPClientInterface) {
        
        self.impl = impl
    }
    
    public private(set) static var `default`: HTTPClientInterface = {
        return alamofire
    }()
    
    public private(set) static var alamofire: HTTPClientInterface = {
        return HTTPClient(impl: AlamofireWrapper())
    }()
    
    public func GET(from url: URL, params: [String : Any]? = nil, headers :[String :String]? = nil,  _ completionHandler: @escaping (HTTPResponse) -> Void) {
        
        let http = self.impl
        DispatchQueue.global(qos: .userInitiated).async {
            
            autoreleasepool(invoking: {
                
                HTTPCallCounter.increment()
                http.GET(from: url, params: params, headers: headers, { (response) in
                    
                    completionHandler(response)
                    HTTPCallCounter.decrement()
                })
            })
        }
    }
    
    public func POST(to url: URL, params: [String : Any]? = nil, images: [String : UIImage]? = nil, headers :[String :String]? = nil, _ completionHandler: @escaping (HTTPResponse) -> Void)  {
        
        let http = self.impl
        DispatchQueue.global(qos: .userInitiated).async {
            
            autoreleasepool(invoking: {
                
                HTTPCallCounter.increment()
                http.POST(to: url, params: params, images: images, headers: headers, { (response) in
                    
                    completionHandler(response)
                    HTTPCallCounter.decrement()
                })
            })
        }
    }
    
    public func PUT(to url: URL, params: [String : Any]? = nil, images: [String : UIImage]? = nil, headers :[String :String]? = nil, _ completionHandler: @escaping (HTTPResponse) -> Void) {
        
        let http = self.impl
        DispatchQueue.global(qos: .userInitiated).async {
            
            autoreleasepool(invoking: {
                
                HTTPCallCounter.increment()
                http.PUT(to: url, params: params, images: images, headers: headers, { (response) in
                    
                    completionHandler(response)
                    HTTPCallCounter.decrement()
                })
            })
        }
        
    }
    
    public func DELETE(to url: URL, params: [String : Any]? = nil, headers :[String :String]? = nil, _ completionHandler: @escaping (HTTPResponse) -> Void) {
        
        let http = self.impl
        DispatchQueue.global(qos: .userInitiated).async {
            
            autoreleasepool(invoking: {
                
                HTTPCallCounter.increment()
                http.DELETE(to: url, params: params, headers: headers, { (response) in
                    
                    completionHandler(response)
                    HTTPCallCounter.decrement()
                })
            })
        }
    }
    
    public func POST(formDataTo url: URL, params: [String : Any]? = nil, images: [String : UIImage]? = nil, headers :[String :String]? = nil, _ completionHandler: @escaping (HTTPResponse) -> Void) {
        
        let http = self.impl
        DispatchQueue.global(qos: .userInitiated).async {
            
            autoreleasepool(invoking: {
                
                HTTPCallCounter.increment()
                http.POST(formDataTo: url, params: params, images: images, headers: headers, { (response) in
                    
                    completionHandler(response)
                    HTTPCallCounter.decrement()
                })
            })
        }
    }
    
    public func POST(multiPartTo url: URL, params: [String : Any]? = nil, images: [String : UIImage]? = nil, progress :HTTPProgress? = nil, headers :[String :String]? = nil, _ completionHandler: @escaping (HTTPResponse) -> Void) {
        
        let http = self.impl
        DispatchQueue.global(qos: .userInitiated).async {
            
            autoreleasepool(invoking: {
                
                HTTPCallCounter.increment()
                http.POST(multiPartTo: url, params: params, images: images, progress: progress, headers: headers, { (response) in
                    
                    completionHandler(response)
                    HTTPCallCounter.decrement()
                })
            })
        }
    }
}







